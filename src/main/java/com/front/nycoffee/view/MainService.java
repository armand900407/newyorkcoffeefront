package com.front.nycoffee.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.print.PrinterException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.project.newyorkcofee.mails.SendClosureMail;
import com.project.newyorkcoffee.bussiness.BalanceCalculating;
import com.project.newyorkcoffee.bussiness.TempSaleCalculating;
import com.project.newyorkcoffee.catalogs.PaymentTypeCatalog;
import com.project.newyorkcoffee.catalogs.ProductSizeCatalog;
import com.project.newyorkcoffee.catalogs.ProductTypesCatalog;
import com.project.newyorkcoffee.client.AdministrationClient;
import com.project.newyorkcoffee.client.CashClient;
import com.project.newyorkcoffee.client.ClosureClient;
import com.project.newyorkcoffee.client.IncomesClient;
import com.project.newyorkcoffee.client.OutgoingsClient;
import com.project.newyorkcoffee.client.ProductClient;
import com.project.newyorkcoffee.client.SalesClient;
import com.project.newyorkcoffee.client.TemporarySaleClient;
import com.project.newyorkcoffee.model.Administration;
import com.project.newyorkcoffee.model.Cash;
import com.project.newyorkcoffee.model.Closure;
import com.project.newyorkcoffee.model.Incomes;
import com.project.newyorkcoffee.model.Outgoings;
import com.project.newyorkcoffee.model.Products;
import com.project.newyorkcoffee.model.SaleTicketModel;
import com.project.newyorkcoffee.model.Sales;
import com.project.newyorkcoffee.model.TemporarySale;
import com.project.newyorkcoffee.print.tickets.ClosureTicket;
import com.project.newyorkcoffee.print.tickets.SaleTicket;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainService extends JFrame {

	/**
	 * Inicio de construccion de los componentes que requieren ser accedidos globalmente para las operaciones locales del controlador front.
	 */
	private static final long serialVersionUID = 963910425063053994L;
	private JPanel contentPane;
	public Image imagenFondo;
	public URL fondo;
	private JPanel panelVenta;
	private JComboBox<String> cmbHotDrinksList;
	private JComboBox<String> cmbColdDrinksList;
	private JComboBox<String> cmbFoodList;
	private JComboBox<String> cmbComplementoBebidas;
	private JComboBox<String> cmbComplementoComidas;
	private JComboBox<String> cmbSalePayType;
	private JTextField txtClientName;
	private TextField txtDrinksQuantity;
	private TextField txtJunkQuantity;
	private TextField txtSaleTotal;
	private TextField txtSaleChange;
	private TextField txtSaleReceived;
	private JScrollPane scrollPaneSale;
	private DefaultTableModel tableModelTempSaleList;
	private JButton btnAddDrink;
	private JButton btnAddJunk;
	private JButton btnEliminiarSeleccionado;
	private JButton btnLimpiarVenta;
	private JButton btnSaleOk;
	private JCheckBox chckBebidaCaliente;
	private JCheckBox chckBebidaFria;
	private JCheckBox chckAgregarCompComida;
	private JCheckBox chckAgregarCompBebida;
	private JTable tblTempSaleList;
	private Products[] hotDrinkList;
	private Products[] coldDrinkList;
	private Products[] foodList;
	private Products[] drinkComplementList;
	private Products[] foodComplementList;
	private TemporarySale[] tempSalePrint;
	
	private int noVenta;
	private double cashQuantity;
	private String employee = "Putito Putin";
	boolean systemAccess=false;
	
	private JTextField txtBalTotalIn;
	private JTextField txtBalTotalOut;
	private JScrollPane spBalEntradas;
	private JScrollPane spBalSalidas;
	private DefaultTableModel tableModelBalanceIn;
	private DefaultTableModel tableModelBalanceOut;
	private JTable tableBalanceIn;
	private JTable tableBalanceOut;
	
	private JTextField txtAltaNombProd;
	private JTextField txtAltaPrecioProd;
	private JScrollPane scrollPanepAltaProductos;
	private DefaultTableModel tableModelAltaProd;
	private JTable tblAltaProductos;
	private JComboBox<String> cmbAltaTipoProd;
	private JComboBox<String> cmbAltaSizeProd;
	
	private JScrollPane spListSaleCheck;
	private JTextField txtNoListaVenta;
	private JTextField txtTotalListaVenta;
	private DefaultTableModel tableModelShowSaleList;
	private JTable tblShowSaleList;
	
	private JTextField txtCorteInicioCaja;
	private JTextField txtCorteTotEfec;
	private JTextField txtCorteTotTarjeta;
	private JTextField txtCorteTotEfeTar;
	private JTextField txtCorteTotGene;
	private JTextField txtCorteSalEfeTar;
	private JTextField txtCorteSalTar;
	private JTextField txtCorteSalEfec;
	private JTextField txtCorteSalCaja;
	

	private JTextField txtOpDescription;
	private JTextField txtOpCantidad;
	private JComboBox<String> cmbOpType;
	private JComboBox<String> cmbAdminOpPayType;
	
	private JTextField txtAdmNombre;
	private JTextField txtAdmApellido;
	private JComboBox<String> cmbEmployeeType;
	private JPasswordField txtPassUno;
	private JTextField txtAdmTel;
	private JPasswordField txtPassDos;
	private DefaultTableModel tableModelAdmin;
	private JTable tblAdministrators;
	private JScrollPane spListAdmin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainService frame = new MainService();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Calls the frame init process.
	 */
	public MainService() {
		//init();
		authenticationProcess();
	}
	
	public void authenticationProcess() {
		Administration admin = new Administration();
		String userName;
		String password=null;
		int passResult;
		JPasswordField pf = new JPasswordField();
		userName=JOptionPane.showInputDialog("Usuario: ");
		passResult=JOptionPane.showConfirmDialog(null, pf, "Contraseña", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (passResult == JOptionPane.OK_OPTION) {
			  password = new String(pf.getPassword());
			}	
		admin = AdministrationClient.getAdminAuthentication(userName,password);
		if(admin != null) {
			systemAccess = true;
			employee = admin.getEmployeeName()+" "+admin.getEmployeeLastName();
			JOptionPane.showMessageDialog(null,"Bienvenid@ " +employee,"NewYork Coffee", JOptionPane.INFORMATION_MESSAGE);
			cashInitializerProcess();
		}else {
			JOptionPane.showMessageDialog(null, "Contraseña Incorrecta","ERROR", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
	}
	
	public void cashInitializerProcess() {
		Cash cash = new Cash();
		double initCash;
		double inCash;
		boolean initCashOk=false;
		cash= CashClient.getCashById(PaymentTypeCatalog.ID_CASH);
		initCash = cash.getQuantity();
		if(initCash==0.0) {
			inCash=Integer.parseInt(JOptionPane.showInputDialog("Inicialize caja por favor: "));
			if(inCash<100) {
			 JOptionPane.showMessageDialog(null,"Debe iniciar caja con minimo de $100 MXN por favor ","NewYork Coffee", JOptionPane.INFORMATION_MESSAGE);
			 System.exit(0);
			}else {
				initCashOk = initCashIncomeProcess(inCash);
				if (initCashOk) {
					cash.setQuantity(inCash);
					cash = null;
					cash = TempSaleCalculating.updateCashProcess(inCash);
				}
				if (cash != null) {
					cashQuantity = cash.getQuantity();
					noVenta = cash.getNoSale();
					init();
				}
			}
		}else {
			cashQuantity = cash.getQuantity();
			noVenta = TempSaleCalculating.getNoSale();
			JOptionPane.showMessageDialog(null,"Verifique cajón : $" +cashQuantity,"NewYork Coffee", JOptionPane.INFORMATION_MESSAGE);
			init();
		}
	}
	
	/**
	 * initCashIncomeProcess executes the initialization cash on process to be stored at data base.
	 */
	public boolean initCashIncomeProcess(double cash) {
		int incomeStatus=200;
		String saleDate;
		boolean initOk=false;
		saleDate = TempSaleCalculating.getCurrentTimeUsingDate();
		Incomes income = new Incomes();
		income.setNoSale(PaymentTypeCatalog.ID_CASH);
		income.setDescription(PaymentTypeCatalog.INIT_CASH);
		income.setPaymentType(PaymentTypeCatalog.CASH);
		income.setTotal(cash);
		income.setIncomeDate(saleDate);	
		income.setEmployee(employee);
		incomeStatus = IncomesClient.addIncome(income);
		if(incomeStatus==200) {
			initOk=true;
		}
		return initOk;
	}
	
	/**
	 * Initialization of frame
	 */
	public void init() {
		setFont(new Font("Liberation Sans", Font.BOLD, 24));
		setResizable(false);
		setTitle("New York Coffee");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1158, 683);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		fondo = this.getClass().getClassLoader().getResource("com/front/nycoffee/images/newyorkTheme.jpg");
		System.out.println("FONDO: "+fondo);
		imagenFondo = new ImageIcon(fondo).getImage();
		panelVenta = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(imagenFondo, 0, 0, getWidth(), getHeight(), this);
			}
			};
		panelVenta.setBackground(SystemColor.control);
		tabbedPane.addTab("Venta", null, panelVenta, null);
		panelVenta.setLayout(null);
				
		//////////////////////////// SECCION DE LABELS DEL PANEL DE VENTA //////////////////////////////
		JLabel lblTituloVenta = new JLabel("NewYork Coffee ");
		lblTituloVenta.setFont(new Font("URW Chancery L", Font.BOLD | Font.ITALIC, 40));
		lblTituloVenta.setForeground(Color.WHITE);
		lblTituloVenta.setBounds(396, 1, 424, 58);
		panelVenta.add(lblTituloVenta);
		
		JLabel lblNewLabel = new JLabel("Venta Bebidas");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 24));
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(48, 29, 205, 30);
		panelVenta.add(lblNewLabel);
		
		JLabel lblComplementoExtra = new JLabel("Complemento extra?");
		lblComplementoExtra.setForeground(Color.WHITE);
		lblComplementoExtra.setFont(new Font("Dialog", Font.BOLD, 18));
		lblComplementoExtra.setBounds(41, 215, 224, 15);
		panelVenta.add(lblComplementoExtra);
		
		JLabel lblCantidad = new JLabel("Cantidad de bebidas:");
		lblCantidad.setForeground(Color.WHITE);
		lblCantidad.setFont(new Font("Dialog", Font.BOLD, 18));
		lblCantidad.setBounds(41, 80, 224, 25);
		panelVenta.add(lblCantidad);
		
		JLabel lblVentaDelicias = new JLabel("Venta Aperitivos");
		lblVentaDelicias.setForeground(Color.WHITE);
		lblVentaDelicias.setFont(new Font("Dialog", Font.BOLD, 24));
		lblVentaDelicias.setBounds(41, 315, 249, 40);
		panelVenta.add(lblVentaDelicias);
		
		JLabel lblCantidadDeAperitivos = new JLabel("Cantidad de aperitivos:");
		lblCantidadDeAperitivos.setForeground(Color.WHITE);
		lblCantidadDeAperitivos.setFont(new Font("Dialog", Font.BOLD, 18));
		lblCantidadDeAperitivos.setBounds(41, 367, 249, 30);
		panelVenta.add(lblCantidadDeAperitivos);
		
		JLabel lblEnVenta = new JLabel("En Venta:");
		lblEnVenta.setForeground(Color.WHITE);
		lblEnVenta.setFont(new Font("Dialog", Font.BOLD, 18));
		lblEnVenta.setBounds(632, 80, 114, 15);
		panelVenta.add(lblEnVenta);
		
		JLabel lblTotal = new JLabel("Total: $");
		lblTotal.setForeground(Color.WHITE);
		lblTotal.setFont(new Font("Dialog", Font.BOLD, 18));
		lblTotal.setBounds(970, 378, 82, 15);
		panelVenta.add(lblTotal);
		
		JLabel lblRecibido = new JLabel(" Recibido: $ ");
		lblRecibido.setForeground(Color.WHITE);
		lblRecibido.setFont(new Font("Dialog", Font.BOLD, 18));
		lblRecibido.setBounds(926, 410, 126, 15);
		panelVenta.add(lblRecibido);
		
		JLabel lblCambio = new JLabel("Cambio: $");
		lblCambio.setForeground(Color.WHITE);
		lblCambio.setFont(new Font("Dialog", Font.BOLD, 18));
		lblCambio.setBounds(949, 436, 103, 15);
		panelVenta.add(lblCambio);
		
		JLabel lblTipoPago = new JLabel("Tipo Pago:");
		lblTipoPago.setForeground(Color.WHITE);
		lblTipoPago.setFont(new Font("Dialog", Font.BOLD, 18));
		lblTipoPago.setBounds(706, 376, 114, 19);
		panelVenta.add(lblTipoPago);
		
		JLabel lblLeAtiende = new JLabel("Le atiende:");
		lblLeAtiende.setForeground(Color.WHITE);
		lblLeAtiende.setFont(new Font("Dialog", Font.BOLD, 18));
		lblLeAtiende.setBounds(724, 570, 126, 19);
		panelVenta.add(lblLeAtiende);
		
		JLabel lblWorkerOn = new JLabel("");
		lblWorkerOn.setForeground(Color.WHITE);
		lblWorkerOn.setBounds(862, 573, 129, 15);
		panelVenta.add(lblWorkerOn);
		lblWorkerOn.setText(employee);
		
		JLabel label_2 = new JLabel("Complemento extra?");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Dialog", Font.BOLD, 18));
		label_2.setBounds(41, 468, 224, 15);
		panelVenta.add(label_2);
		
		JLabel lblANombreDe = new JLabel("A nombre de:");
		lblANombreDe.setForeground(Color.WHITE);
		lblANombreDe.setFont(new Font("Dialog", Font.BOLD, 18));
		lblANombreDe.setBounds(300, 239, 143, 29);
		panelVenta.add(lblANombreDe);
		///////////////////////////////// TERMINAN LABELS DE PANEL DE VENTA //////////////////////////////////////////////
		
		/////////////////////////// INICIA CONSTRUCCION PARA COMBOS DEL PANEL VENTA /////////////////////////////////////////////////////
		
		cmbHotDrinksList = new JComboBox<String>();
		cmbHotDrinksList.setBounds(41, 166, 271, 24);
		panelVenta.add(cmbHotDrinksList);
		
		cmbColdDrinksList = new JComboBox<String>();
		cmbColdDrinksList.setBounds(334, 166, 271, 24);
		panelVenta.add(cmbColdDrinksList);
		
		cmbFoodList = new JComboBox<String>();
		cmbFoodList.setBounds(41, 409, 321, 24);
		panelVenta.add(cmbFoodList);
		
		cmbComplementoBebidas = new JComboBox<String>();
		cmbComplementoBebidas.setBounds(41, 242, 249, 24);
		panelVenta.add(cmbComplementoBebidas);
		
		cmbComplementoComidas = new JComboBox<String>();
		cmbComplementoComidas.setBounds(41, 506, 321, 24);
		panelVenta.add(cmbComplementoComidas);
		
		cmbSalePayType = new JComboBox<String>();
		cmbSalePayType.setBounds(699, 401, 160, 24);
		panelVenta.add(cmbSalePayType);
		cmbSalePayType.addItem(PaymentTypeCatalog.CASH);
		cmbSalePayType.addItem(PaymentTypeCatalog.CARD);
		
		initCombosProcess();
		
//////////////////////////////// TERMINA LA CONSTRUCCION DE LOS COMBOS DE VENTA //////////////////////////////
		
		
//////////////////////////// INICIA CONSTRUCCION DE CAMPOS DE TEXTO  DE PANEL DE VENTA //////////////////////////////////////////////////////
		txtDrinksQuantity = new TextField();
		txtDrinksQuantity.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				drinkQuantityProcess();
			}
		});
		txtDrinksQuantity.setText("1");
		txtDrinksQuantity.setBounds(269, 75, 43, 30);
		panelVenta.add(txtDrinksQuantity);
		
		txtJunkQuantity = new TextField();
		txtJunkQuantity.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				junkQuantityProcess();
			}
		});
		txtJunkQuantity.setText("1");
		txtJunkQuantity.setBounds(296, 366, 43, 31);
		panelVenta.add(txtJunkQuantity);
		
		txtSaleTotal = new TextField();
		txtSaleTotal.setEnabled(false);
		txtSaleTotal.setText("0.0");
		txtSaleTotal.setBounds(1051, 374, 60, 25);
		panelVenta.add(txtSaleTotal);
		
		txtSaleChange = new TextField();
		txtSaleChange.setText("0.0");
		txtSaleChange.setEnabled(false);
		txtSaleChange.setBounds(1050, 432, 60, 25);
		panelVenta.add(txtSaleChange);
		
		txtSaleReceived = new TextField();
		txtSaleReceived.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtSaleReceived.setText(null);
			}
		});
		txtSaleReceived.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				saleReceivedProcess();
			}
		});
		txtSaleReceived.setText("0.0");
		txtSaleReceived.setBounds(1050, 401, 60, 25);
		panelVenta.add(txtSaleReceived);
		
		txtClientName = new JTextField();
		txtClientName.setColumns(10);
		txtClientName.setBounds(443, 240, 140, 30);
		panelVenta.add(txtClientName);
		////////////////////// TERMINA LA CONSTRUCCION DE LOS CAMPOS DE TEXTO DE VENTA /////////////////////////////
		
///////////////// INICIA CONSTRUCCION PARA CHECKBOXS DE VENTA ////////////////////////////
		chckBebidaCaliente = new JCheckBox("Bebida Caliente");
		chckBebidaCaliente.setBounds(41, 135, 160, 23);
		panelVenta.add(chckBebidaCaliente);
		
		chckBebidaFria = new JCheckBox("Bebida Fria");
		chckBebidaFria.setBounds(339, 135, 129, 23);
		panelVenta.add(chckBebidaFria);
		
		chckAgregarCompComida = new JCheckBox("Agregar");
		chckAgregarCompComida.setBounds(269, 465, 129, 23);
		panelVenta.add(chckAgregarCompComida);
		
		chckAgregarCompBebida = new JCheckBox("Agregar");
		chckAgregarCompBebida.setBounds(269, 211, 129, 23);
		panelVenta.add(chckAgregarCompBebida);
////////////////////// TERMINA CONSTRUCCION DE CHECKBOXS DE VENTA ///////////////////////////

///////////////////// INICIA CONSTRUCCION PARA EL TAB DE VENTA /////////////////////////////////////////////
		scrollPaneSale = new JScrollPane();
		scrollPaneSale.setBounds(632, 113, 499, 255);
		panelVenta.add(scrollPaneSale);

///////////// CREACION DE LA TABLA DE VENTAS DINAMICA ///////////////////////////
		final String saleColumns[] = { "Id", "Producto", "Cantidad", "Total" };
		tableModelTempSaleList = new DefaultTableModel(saleColumns, 0);
		tblTempSaleList = new JTable(tableModelTempSaleList);
		tblTempSaleList.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tblTempSaleList.getColumnModel().getColumn(0).setPreferredWidth(50);
		tblTempSaleList.getColumnModel().getColumn(1).setPreferredWidth(290);
		tblTempSaleList.getColumnModel().getColumn(2).setPreferredWidth(80);
		tblTempSaleList.getColumnModel().getColumn(3).setPreferredWidth(80);
		TemporarySale[] tempSale; //MODIFICAR
		tempSale = TemporarySaleClient.getAllTempSales();
		for (TemporarySale temporarySale : tempSale) {
			Object[] saleItems = { temporarySale.getIdTempSale(), temporarySale.getDescription(),
					temporarySale.getQuantity(), temporarySale.getTotal() };
			tableModelTempSaleList.addRow(saleItems);
		}
		txtSaleTotal.setText(TempSaleCalculating.getTotalTempSale(tempSale));
		scrollPaneSale.setViewportView(tblTempSaleList);
///////////////////////////////////// TERMINA CONSTRUCCION DE TABLA VENTA /////////////////////////////////////		

///////////////////////////////// INICIO DE BOTONES Y EVENTOS PARA VENTA //////////////////////////////////////
		btnAddDrink = new JButton("Agregar Bebida!");
		btnAddDrink.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addDrinkProcess();
			}
		});
		btnAddDrink.setForeground(Color.WHITE);
		btnAddDrink.setBackground(new Color(50, 205, 50));
		btnAddDrink.setBounds(411, 287, 172, 39);
		panelVenta.add(btnAddDrink);

		// EVENTO DEL BOTON DE AGREGAR APERITIVOS
		btnAddJunk = new JButton("Agregar Aperitivo!");
		btnAddJunk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addFoodProcess();
			}
		});
		btnAddJunk.setForeground(Color.WHITE);
		btnAddJunk.setBackground(new Color(50, 205, 50));
		btnAddJunk.setBounds(411, 545, 172, 41);
		panelVenta.add(btnAddJunk);

		// Boton para eliminar producto seleccionado por el usuario
		btnEliminiarSeleccionado = new JButton("Eliminiar Seleccionado");
		btnEliminiarSeleccionado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeSelectedSaleProcess();
			}
		});
		btnEliminiarSeleccionado.setForeground(Color.WHITE);
		btnEliminiarSeleccionado.setBackground(new Color(220, 20, 60));
		btnEliminiarSeleccionado.setBounds(928, 69, 203, 39);
		panelVenta.add(btnEliminiarSeleccionado);

		// Boton para limpiar la tabla de ventas en caso de ser requerido
		btnLimpiarVenta = new JButton("Limpiar Venta");
		btnLimpiarVenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cleanTempTableProcess();
			}
		});
		btnLimpiarVenta.setForeground(Color.WHITE);
		btnLimpiarVenta.setBackground(Color.BLACK);
		btnLimpiarVenta.setBounds(751, 69, 172, 39);
		panelVenta.add(btnLimpiarVenta);

		btnSaleOk = new JButton("Realizar Venta!");
		btnSaleOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					executeSaleProcess();
				} catch (PrinterException e1) {
					System.out.println("Problemas al intentar imprimir ticket de venta");
					e1.printStackTrace();
				} catch (InterruptedException e1) {
					System.out.println("Problemas en el hilo de ejecucion para impresiones");
					e1.printStackTrace();
				}
			}
		});
		btnSaleOk.setForeground(Color.WHITE);
		btnSaleOk.setBackground(new Color(0, 255, 0));
		btnSaleOk.setBounds(950, 489, 160, 39);
		panelVenta.add(btnSaleOk);
		
		JButton btnConsultaCajn = new JButton("Consulta Cajón");
		btnConsultaCajn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getCashInfoProcess();
			}
		});
		btnConsultaCajn.setForeground(Color.WHITE);
		btnConsultaCajn.setBackground(Color.BLUE);
		btnConsultaCajn.setBounds(988, 568, 143, 25);
		panelVenta.add(btnConsultaCajn);
		
////////////////////////////////// TERMINA CONSTRUCCION DE BOTONES DE PANEL DE VENTA ////////////////////////////////////
		
////////////////////////////////// INICIA CONSTRUCCION PARA EL TAB DE PRODUCTO /////////////////////////////////////////
		JPanel panelProducto = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(imagenFondo, 0, 0, getWidth(), getHeight(),this);
			}	
		};
		tabbedPane.addTab("Productos", null, panelProducto, null);
		panelProducto.setLayout(null);
		
		JLabel label = new JLabel("NewYork Coffee ");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("URW Chancery L", Font.BOLD | Font.ITALIC, 40));
		label.setBounds(379, 12, 444, 58);
		panelProducto.add(label);
		
		JLabel lblAltaDeProductos = new JLabel("Alta de productos");
		lblAltaDeProductos.setForeground(Color.WHITE);
		lblAltaDeProductos.setFont(new Font("Dialog", Font.BOLD, 24));
		lblAltaDeProductos.setBounds(139, 85, 242, 30);
		panelProducto.add(lblAltaDeProductos);
		
		JLabel lblConsultaProductos = new JLabel("Consulta Productos");
		lblConsultaProductos.setForeground(Color.WHITE);
		lblConsultaProductos.setFont(new Font("Dialog", Font.BOLD, 24));
		lblConsultaProductos.setBounds(620, 85, 453, 30);
		panelProducto.add(lblConsultaProductos);
		
		JLabel lblNombreProducto = new JLabel("Nombre Producto:");
		lblNombreProducto.setForeground(Color.WHITE);
		lblNombreProducto.setFont(new Font("Dialog", Font.BOLD, 16));
		lblNombreProducto.setBounds(25, 185, 190, 19);
		panelProducto.add(lblNombreProducto);
		
		JLabel lblTipoProducto = new JLabel("TIpo Producto:");
		lblTipoProducto.setForeground(Color.WHITE);
		lblTipoProducto.setFont(new Font("Dialog", Font.BOLD, 16));
		lblTipoProducto.setBounds(57, 247, 133, 19);
		panelProducto.add(lblTipoProducto);
		
		JLabel lblProductSize = new JLabel("Tamaño:");
		lblProductSize.setForeground(Color.WHITE);
		lblProductSize.setFont(new Font("Dialog", Font.BOLD, 16));
		lblProductSize.setBounds(110, 301, 80, 19);
		panelProducto.add(lblProductSize);
		
		JLabel lblPrecio = new JLabel("Precio: $");
		lblPrecio.setForeground(Color.WHITE);
		lblPrecio.setFont(new Font("Dialog", Font.BOLD, 16));
		lblPrecio.setBounds(110, 367, 80, 19);
		panelProducto.add(lblPrecio);
		
		txtAltaNombProd = new JTextField();
		txtAltaNombProd.setBounds(214, 181, 200, 30);
		panelProducto.add(txtAltaNombProd);
		txtAltaNombProd.setColumns(10);
		
		cmbAltaTipoProd = new JComboBox<String>();
		cmbAltaTipoProd.setBounds(213, 245, 190, 24);
		panelProducto.add(cmbAltaTipoProd);
		cmbAltaTipoProd.addItem(ProductTypesCatalog.HOT_DRINK);
		cmbAltaTipoProd.addItem(ProductTypesCatalog.COLD_DRINK);
		cmbAltaTipoProd.addItem(ProductTypesCatalog.FOOD);
		cmbAltaTipoProd.addItem(ProductTypesCatalog.FOOD_COMPLEMENT);
		cmbAltaTipoProd.addItem(ProductTypesCatalog.DRINK_COMPLEMENT);
		
		
		cmbAltaSizeProd = new JComboBox<String>();
		cmbAltaSizeProd.setBounds(214, 299, 151, 24);
		panelProducto.add(cmbAltaSizeProd);
		cmbAltaSizeProd.addItem(ProductSizeCatalog.SMALL_SIZE);
		cmbAltaSizeProd.addItem(ProductSizeCatalog.MEDIUM_SIZE);
		cmbAltaSizeProd.addItem(ProductSizeCatalog.BIG_SIZE);
		cmbAltaSizeProd.addItem(ProductSizeCatalog.X_BIG_SIZE);
		cmbAltaSizeProd.addItem(ProductSizeCatalog.NORMAL);
		
		txtAltaPrecioProd = new JTextField();
		txtAltaPrecioProd.setBounds(214, 363, 61, 30);
		panelProducto.add(txtAltaPrecioProd);
		txtAltaPrecioProd.setColumns(10);
		
		JButton btnGuardarProducto = new JButton("Guardar Producto");
		btnGuardarProducto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNewProductProcess();
			}
		});
		btnGuardarProducto.setForeground(Color.WHITE);
		btnGuardarProducto.setBackground(new Color(50, 205, 50));
		btnGuardarProducto.setBounds(174, 460, 172, 41);
		panelProducto.add(btnGuardarProducto);
		
		scrollPanepAltaProductos = new JScrollPane();
		scrollPanepAltaProductos.setBounds(542, 147, 575, 365);
		panelProducto.add(scrollPanepAltaProductos);
		
		tblAltaProductos = new JTable();
		scrollPanepAltaProductos.setColumnHeaderView(tblAltaProductos);
		
		JButton btnMostrarTodos = new JButton("Mostrar Todos");
		btnMostrarTodos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showAllProductsProcess();
			}
		});
		btnMostrarTodos.setForeground(Color.WHITE);
		btnMostrarTodos.setBackground(new Color(255, 165, 0));
		btnMostrarTodos.setBounds(759, 524, 137, 41);
		panelProducto.add(btnMostrarTodos);
		
		JButton btnActualizar = new JButton("Actualizar");
		btnActualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateSelectedProductProcess();
			}
		});
		btnActualizar.setForeground(Color.WHITE);
		btnActualizar.setBackground(Color.BLUE);
		btnActualizar.setBounds(577, 524, 111, 30);
		panelProducto.add(btnActualizar);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeSelectedProductProcess();
			}
		});
		btnEliminar.setForeground(Color.WHITE);
		btnEliminar.setBackground(Color.RED);
		btnEliminar.setBounds(962, 524, 111, 30);
		panelProducto.add(btnEliminar);
		
		///// INICA CONSTRUCCION DE TABLA PARA AÑADIR PRODUCTOS NUEVOS //////MODIFICAR
		final String newProdColumns[] = { "Id", "Producto", "Tipo Producto", "Tamaño","Precio" };
		tableModelAltaProd = new DefaultTableModel(newProdColumns, 0);
		tblAltaProductos = new JTable(tableModelAltaProd);
		tblAltaProductos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tblAltaProductos.getColumnModel().getColumn(0).setPreferredWidth(30);
		tblAltaProductos.getColumnModel().getColumn(1).setPreferredWidth(210);
		tblAltaProductos.getColumnModel().getColumn(2).setPreferredWidth(180);
		tblAltaProductos.getColumnModel().getColumn(3).setPreferredWidth(100);
		tblAltaProductos.getColumnModel().getColumn(3).setPreferredWidth(100);
		Products[] products;
		products = ProductClient.getAllProducts();
		for (Products prods : products) {
			Object[] prodItems = { prods.getIdProduct(), prods.getProductName(),
					prods.getProductType(), prods.getSize(),prods.getPrice() };
			tableModelAltaProd.addRow(prodItems);
		}
		scrollPanepAltaProductos.setViewportView(tblAltaProductos);
		
///////////////////////////////// INICIA CONSTRUCCION PARA EL TAB DE BALANCE /////////////////////////////////////////////
		JPanel panelBalance = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(imagenFondo, 0, 0, getWidth(), getHeight(),this);
			}	
		};
		tabbedPane.addTab("Balance", null, panelBalance, null);
		panelBalance.setLayout(null);
		
		spBalEntradas = new JScrollPane();
		spBalEntradas.setBounds(31, 109, 525, 400);
		panelBalance.add(spBalEntradas);
		
		tableBalanceIn = new JTable();
		spBalEntradas.setColumnHeaderView(tableBalanceIn);
		
		spBalSalidas = new JScrollPane();
		spBalSalidas.setBounds(583, 109, 525, 400);
		panelBalance.add(spBalSalidas);
		
		tableBalanceOut = new JTable();
		spBalSalidas.setColumnHeaderView(tableBalanceOut);
		
		JLabel label_1 = new JLabel("NewYork Coffee ");
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("URW Chancery L", Font.BOLD | Font.ITALIC, 40));
		label_1.setBounds(432, 12, 431, 58);
		panelBalance.add(label_1);
		
		JLabel lblTotalEntradas = new JLabel("Total Entradas:");
		lblTotalEntradas.setForeground(Color.WHITE);
		lblTotalEntradas.setFont(new Font("Dialog", Font.BOLD, 18));
		lblTotalEntradas.setBounds(98, 521, 161, 30);
		panelBalance.add(lblTotalEntradas);
		
		JLabel lblTotalSalidas = new JLabel("Total Salidas:");
		lblTotalSalidas.setForeground(Color.WHITE);
		lblTotalSalidas.setFont(new Font("Dialog", Font.BOLD, 18));
		lblTotalSalidas.setBounds(716, 521, 147, 30);
		panelBalance.add(lblTotalSalidas);
		
		JLabel lblBalance = new JLabel("Balance");
		lblBalance.setForeground(Color.WHITE);
		lblBalance.setFont(new Font("URW Chancery L", Font.BOLD | Font.ITALIC, 40));
		lblBalance.setBounds(504, 58, 216, 58);
		panelBalance.add(lblBalance);
		
		JLabel lblEntradas = new JLabel("Entradas");
		lblEntradas.setForeground(Color.WHITE);
		lblEntradas.setFont(new Font("URW Chancery L", Font.BOLD | Font.ITALIC, 30));
		lblEntradas.setBounds(71, 58, 188, 39);
		panelBalance.add(lblEntradas);
		
		JLabel lblSalidas = new JLabel("Salidas");
		lblSalidas.setForeground(Color.WHITE);
		lblSalidas.setFont(new Font("URW Chancery L", Font.BOLD | Font.ITALIC, 30));
		lblSalidas.setBounds(952, 54, 136, 47);
		panelBalance.add(lblSalidas);
		
		txtBalTotalIn = new JTextField();
		txtBalTotalIn.setForeground(Color.BLACK);
		txtBalTotalIn.setFont(new Font("Dialog", Font.BOLD, 12));
		txtBalTotalIn.setEnabled(false);
		txtBalTotalIn.setBounds(277, 521, 52, 30);
		panelBalance.add(txtBalTotalIn);
		txtBalTotalIn.setColumns(10);
		
		txtBalTotalOut = new JTextField();
		txtBalTotalOut.setForeground(Color.BLACK);
		txtBalTotalOut.setFont(new Font("Dialog", Font.BOLD, 12));
		txtBalTotalOut.setEnabled(false);
		txtBalTotalOut.setColumns(10);
		txtBalTotalOut.setBounds(869, 523, 52, 30);
		panelBalance.add(txtBalTotalOut);
		
///////////// CREACION DE LA TABLA DE BALANCE ENTRADAS ///////////////////////////
	final String IncomesColumns[] = { "Id", "No. Venta", "Descripción", "Tipo Pago", "Total","Fecha/Hora","Atendió" };
	tableModelBalanceIn = new DefaultTableModel(IncomesColumns, 0);
	tableBalanceIn = new JTable(tableModelBalanceIn);
	tableBalanceIn.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	tableBalanceIn.getColumnModel().getColumn(0).setPreferredWidth(20);
	tableBalanceIn.getColumnModel().getColumn(1).setPreferredWidth(50);
	tableBalanceIn.getColumnModel().getColumn(2).setPreferredWidth(220);
	tableBalanceIn.getColumnModel().getColumn(3).setPreferredWidth(80);
	tableBalanceIn.getColumnModel().getColumn(4).setPreferredWidth(80);
	tableBalanceIn.getColumnModel().getColumn(5).setPreferredWidth(80);
	tableBalanceIn.getColumnModel().getColumn(6).setPreferredWidth(80);
	
///////////// CREACION DE LA TABLA DE BALANCE SALIDAS ///////////////////////////
		final String OutGoingssColumns[] = { "Id","Descripción", "Tipo Pago", "Total", "Fecha/Hora","Atendió" };
		tableModelBalanceOut = new DefaultTableModel(OutGoingssColumns, 0);
		tableBalanceOut = new JTable(tableModelBalanceOut);
		tableBalanceOut.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tableBalanceOut.getColumnModel().getColumn(0).setPreferredWidth(30);
		tableBalanceOut.getColumnModel().getColumn(1).setPreferredWidth(220);
		tableBalanceOut.getColumnModel().getColumn(2).setPreferredWidth(80);
		tableBalanceOut.getColumnModel().getColumn(3).setPreferredWidth(80);
		tableBalanceOut.getColumnModel().getColumn(4).setPreferredWidth(80);
		tableBalanceOut.getColumnModel().getColumn(5).setPreferredWidth(80);
		
		initBalanceProcess();
		
		//// CONSTRUCCION DE BOTON DE BALANCE //////
		JButton btnBalance = new JButton("Balance");
		btnBalance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generateBalanceProcess(true);
			}
		});
		btnBalance.setForeground(Color.WHITE);
		btnBalance.setBackground(new Color(0, 0, 205));
		btnBalance.setBounds(519, 546, 105, 39);
		panelBalance.add(btnBalance);

/////////////////////////////TERMINA CONSTRUCCION DE BALANCE /////////////////////////////////////////
		
/////////////////////////////INICIA CONSTRUCCION PARA EL TAB DE LISTADO DE VENTAS /////////////////////////////////////////
		JPanel panelListadoVentas = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(imagenFondo, 0, 0, getWidth(), getHeight(),this);
			}	
		};
		tabbedPane.addTab("Lista de Ventas", null, panelListadoVentas, null);
		panelListadoVentas.setLayout(null);
		
		JLabel label_3 = new JLabel("NewYork Coffee ");
		label_3.setForeground(Color.WHITE);
		label_3.setFont(new Font("URW Chancery L", Font.BOLD | Font.ITALIC, 40));
		label_3.setBounds(408, 12, 427, 58);
		panelListadoVentas.add(label_3);
		
		spListSaleCheck = new JScrollPane();
		spListSaleCheck.setBounds(70, 156, 663, 367);
		panelListadoVentas.add(spListSaleCheck);
		
		tblShowSaleList = new JTable();
		spListSaleCheck.setColumnHeaderView(tblShowSaleList);
		
		JLabel lblListadoDeVentas = new JLabel("Listado de Ventas");
		lblListadoDeVentas.setForeground(Color.WHITE);
		lblListadoDeVentas.setFont(new Font("URW Chancery L", Font.BOLD | Font.ITALIC, 30));
		lblListadoDeVentas.setBounds(70, 62, 326, 39);
		panelListadoVentas.add(lblListadoDeVentas);
		
		JLabel lblNoVenta = new JLabel("No. Venta:");
		lblNoVenta.setForeground(Color.WHITE);
		lblNoVenta.setFont(new Font("Dialog", Font.BOLD, 18));
		lblNoVenta.setBounds(70, 113, 117, 30);
		panelListadoVentas.add(lblNoVenta);
		
		JLabel lblTotalVenta = new JLabel("Total Venta:");
		lblTotalVenta.setForeground(Color.WHITE);
		lblTotalVenta.setFont(new Font("Dialog", Font.BOLD, 18));
		lblTotalVenta.setBounds(506, 535, 133, 30);
		panelListadoVentas.add(lblTotalVenta);
		
		txtTotalListaVenta = new JTextField();
		txtTotalListaVenta.setEnabled(false);
		txtTotalListaVenta.setBounds(647, 535, 58, 30);
		panelListadoVentas.add(txtTotalListaVenta);
		txtTotalListaVenta.setColumns(10);
		
		JButton btnCancelarVenta = new JButton("Cancelar Venta");
		btnCancelarVenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saleCancellationProcess(txtNoListaVenta.getText());
			}
		});
		btnCancelarVenta.setForeground(Color.WHITE);
		btnCancelarVenta.setBackground(new Color(220, 20, 60));
		btnCancelarVenta.setBounds(842, 249, 153, 39);
		panelListadoVentas.add(btnCancelarVenta);
		
		JButton btnMostrarTodoVenta = new JButton("Mostrar Todo");
		btnMostrarTodoVenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNoListaVenta.setText(null);
				initListSaleCheckProcess();
			}
		});
		btnMostrarTodoVenta.setForeground(Color.WHITE);
		btnMostrarTodoVenta.setBackground(new Color(0, 0, 205));
		btnMostrarTodoVenta.setBounds(842, 351, 153, 39);
		panelListadoVentas.add(btnMostrarTodoVenta);
		
		final String saleCheckColumns[] = {"No.Venta","Descripción", "Tipo Pago", "Total", "Fecha/Hora"};
		tableModelShowSaleList = new DefaultTableModel(saleCheckColumns, 0);
		tblShowSaleList = new JTable(tableModelShowSaleList);
		tblShowSaleList.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tblShowSaleList.getColumnModel().getColumn(0).setPreferredWidth(50);
		tblShowSaleList.getColumnModel().getColumn(1).setPreferredWidth(280);
		tblShowSaleList.getColumnModel().getColumn(2).setPreferredWidth(90);
		tblShowSaleList.getColumnModel().getColumn(3).setPreferredWidth(80);
		tblShowSaleList.getColumnModel().getColumn(4).setPreferredWidth(200);
		initListSaleCheckProcess();
		
		txtNoListaVenta = new JTextField();
		txtNoListaVenta.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				searchNoSaleProcess(txtNoListaVenta.getText());
			}
		});
		txtNoListaVenta.setBounds(188, 113, 49, 31);
		panelListadoVentas.add(txtNoListaVenta);
		txtNoListaVenta.setColumns(10);
		
		
		
		
		
		
//////////////////////////// INICIA CONSTRUCCION PARA EL TAB DE CORTE //////////////////////
		JPanel panelCorte = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(imagenFondo, 0, 0, getWidth(), getHeight(),this);
			}	
		};
		tabbedPane.addTab("Corte", null, panelCorte, null);
		panelCorte.setLayout(null);
		
		JLabel label_4 = new JLabel("NewYork Coffee ");
		label_4.setForeground(Color.WHITE);
		label_4.setFont(new Font("URW Chancery L", Font.BOLD | Font.ITALIC, 40));
		panelCorte.add(label_4);
		
		JLabel label_5 = new JLabel("NewYork Coffee ");
		label_5.setForeground(Color.WHITE);
		label_5.setFont(new Font("URW Chancery L", Font.BOLD | Font.ITALIC, 40));
		label_5.setBounds(423, 12, 420, 58);
		panelCorte.add(label_5);
		
		JLabel lblCorteDeCaja = new JLabel("Corte de Caja");
		lblCorteDeCaja.setBackground(new Color(238, 238, 238));
		lblCorteDeCaja.setForeground(Color.WHITE);
		lblCorteDeCaja.setFont(new Font("URW Chancery L", Font.BOLD | Font.ITALIC, 34));
		lblCorteDeCaja.setBounds(446, 82, 281, 39);
		panelCorte.add(lblCorteDeCaja);
		
		JLabel lblInicioDeCaja = new JLabel(" Inicio de Caja: $");
		lblInicioDeCaja.setForeground(Color.WHITE);
		lblInicioDeCaja.setFont(new Font("Dialog", Font.BOLD, 18));
		lblInicioDeCaja.setBounds(166, 182, 167, 30);
		panelCorte.add(lblInicioDeCaja);
		
		JLabel lblTotalEfectivo = new JLabel("Total Efectivo: $");
		lblTotalEfectivo.setForeground(Color.WHITE);
		lblTotalEfectivo.setFont(new Font("Dialog", Font.BOLD, 18));
		lblTotalEfectivo.setBounds(166, 257, 167, 30);
		panelCorte.add(lblTotalEfectivo);
		
		JLabel lblTotalTarjeta = new JLabel("Total Tarjeta: $");
		lblTotalTarjeta.setForeground(Color.WHITE);
		lblTotalTarjeta.setFont(new Font("Dialog", Font.BOLD, 18));
		lblTotalTarjeta.setBounds(177, 320, 156, 30);
		panelCorte.add(lblTotalTarjeta);
		
		JLabel lblTotalNetoGenerado = new JLabel("Total Neto Generado: $");
		lblTotalNetoGenerado.setForeground(Color.WHITE);
		lblTotalNetoGenerado.setFont(new Font("Dialog", Font.BOLD, 18));
		lblTotalNetoGenerado.setBounds(268, 447, 240, 30);
		panelCorte.add(lblTotalNetoGenerado);
		
		JLabel lblEntradas_1 = new JLabel("Entradas");
		lblEntradas_1.setForeground(Color.WHITE);
		lblEntradas_1.setFont(new Font("Dialog", Font.BOLD, 24));
		lblEntradas_1.setBounds(210, 119, 147, 30);
		panelCorte.add(lblEntradas_1);
		
		JLabel lblSalidas_1 = new JLabel("Salidas");
		lblSalidas_1.setForeground(Color.WHITE);
		lblSalidas_1.setFont(new Font("Dialog", Font.BOLD, 24));
		lblSalidas_1.setBounds(782, 119, 147, 30);
		panelCorte.add(lblSalidas_1);
		
		JLabel lblSalidasEfectivo = new JLabel("Salidas Efectivo: $");
		lblSalidasEfectivo.setForeground(Color.WHITE);
		lblSalidasEfectivo.setFont(new Font("Dialog", Font.BOLD, 18));
		lblSalidasEfectivo.setBounds(675, 257, 191, 30);
		panelCorte.add(lblSalidasEfectivo);
		
		JLabel lblSalidasTarjeta = new JLabel("Salidas Tarjeta: $");
		lblSalidasTarjeta.setForeground(Color.WHITE);
		lblSalidasTarjeta.setFont(new Font("Dialog", Font.BOLD, 18));
		lblSalidasTarjeta.setBounds(683, 320, 183, 30);
		panelCorte.add(lblSalidasTarjeta);
		
		JLabel lblEfectivoYTarjeta = new JLabel("Efectivo y Tarjeta: $");
		lblEfectivoYTarjeta.setForeground(Color.WHITE);
		lblEfectivoYTarjeta.setFont(new Font("Dialog", Font.BOLD, 18));
		lblEfectivoYTarjeta.setBounds(123, 382, 211, 30);
		panelCorte.add(lblEfectivoYTarjeta);
		
		JLabel lblEfectivoYTarjeta_1 = new JLabel("Efectivo y Tarjeta: $");
		lblEfectivoYTarjeta_1.setForeground(Color.WHITE);
		lblEfectivoYTarjeta_1.setFont(new Font("Dialog", Font.BOLD, 18));
		lblEfectivoYTarjeta_1.setBounds(659, 377, 207, 30);
		panelCorte.add(lblEfectivoYTarjeta_1);
		
		JLabel lblCorteDeCaja_1 = new JLabel("Cierre de Caja: $");
		lblCorteDeCaja_1.setForeground(Color.WHITE);
		lblCorteDeCaja_1.setFont(new Font("Dialog", Font.BOLD, 18));
		lblCorteDeCaja_1.setBounds(691, 182, 175, 30);
		panelCorte.add(lblCorteDeCaja_1);
		
		txtCorteInicioCaja = new JTextField();
		txtCorteInicioCaja.setEnabled(false);
		txtCorteInicioCaja.setBounds(331, 184, 62, 30);
		panelCorte.add(txtCorteInicioCaja);
		txtCorteInicioCaja.setColumns(10);
		
		txtCorteTotEfec = new JTextField();
		txtCorteTotEfec.setEnabled(false);
		txtCorteTotEfec.setColumns(10);
		txtCorteTotEfec.setBounds(331, 257, 62, 30);
		panelCorte.add(txtCorteTotEfec);
		
		txtCorteTotTarjeta = new JTextField();
		txtCorteTotTarjeta.setEnabled(false);
		txtCorteTotTarjeta.setColumns(10);
		txtCorteTotTarjeta.setBounds(331, 320, 62, 30);
		panelCorte.add(txtCorteTotTarjeta);
		
		txtCorteTotEfeTar = new JTextField();
		txtCorteTotEfeTar.setEnabled(false);
		txtCorteTotEfeTar.setColumns(10);
		txtCorteTotEfeTar.setBounds(331, 384, 62, 30);
		panelCorte.add(txtCorteTotEfeTar);
		
		txtCorteTotGene = new JTextField();
		txtCorteTotGene.setEnabled(false);
		txtCorteTotGene.setColumns(10);
		txtCorteTotGene.setBounds(509, 449, 62, 30);
		panelCorte.add(txtCorteTotGene);
		
		txtCorteSalEfeTar = new JTextField();
		txtCorteSalEfeTar.setEnabled(false);
		txtCorteSalEfeTar.setColumns(10);
		txtCorteSalEfeTar.setBounds(867, 379, 62, 30);
		panelCorte.add(txtCorteSalEfeTar);
		
		txtCorteSalTar = new JTextField();
		txtCorteSalTar.setEnabled(false);
		txtCorteSalTar.setColumns(10);
		txtCorteSalTar.setBounds(867, 320, 62, 30);
		panelCorte.add(txtCorteSalTar);
		
		txtCorteSalEfec = new JTextField();
		txtCorteSalEfec.setEnabled(false);
		txtCorteSalEfec.setColumns(10);
		txtCorteSalEfec.setBounds(867, 257, 62, 30);
		panelCorte.add(txtCorteSalEfec);
		
		txtCorteSalCaja = new JTextField();
		txtCorteSalCaja.setEnabled(false);
		txtCorteSalCaja.setColumns(10);
		txtCorteSalCaja.setBounds(867, 182, 62, 30);
		panelCorte.add(txtCorteSalCaja);
	
		JButton btnCalcularCorte = new JButton("Calcular Corte");
		btnCalcularCorte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calculateClosureProcess();
			}
		});
		btnCalcularCorte.setForeground(Color.WHITE);
		btnCalcularCorte.setBackground(new Color(65, 105, 225));
		btnCalcularCorte.setBounds(468, 524, 159, 40);
		panelCorte.add(btnCalcularCorte);
		
		JButton btnLimpiarCorte = new JButton("Limpiar");
		btnLimpiarCorte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cleanClosureProcess();
			}
		});
		btnLimpiarCorte.setForeground(Color.WHITE);
		btnLimpiarCorte.setBackground(new Color(255, 165, 0));
		btnLimpiarCorte.setBounds(96, 526, 159, 36);
		panelCorte.add(btnLimpiarCorte);
		
		JButton btnGenerarCorte = new JButton("Generar Corte");
		btnGenerarCorte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					closureGenerationProcess();
				} catch (PrinterException e1) {
					System.out.println("Problemas al imprimir ticket de Cierre: "+e1);
					e1.printStackTrace();
				} catch (Exception e1) {
					System.out.println("Problemas al descifrar usuario y password: "+e1);
					e1.printStackTrace();
				}
			}
		});
		btnGenerarCorte.setForeground(Color.WHITE);
		btnGenerarCorte.setBackground(new Color(50, 205, 50));
		btnGenerarCorte.setBounds(895, 525, 159, 39);
		panelCorte.add(btnGenerarCorte);
		
/////////////////////////////////////// INICIA CONSTRUCCION PARA EL TAB DE OPERACIONES ADM //////////////////////
		JPanel panelOperaciones = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(imagenFondo, 0, 0, getWidth(), getHeight(), this);
			}
			};
		tabbedPane.addTab("Operaciones Adm", null, panelOperaciones, null);
		panelOperaciones.setLayout(null);
		
		JLabel label_7 = new JLabel("NewYork Coffee ");
		label_7.setForeground(Color.WHITE);
		label_7.setFont(new Font("URW Chancery L", Font.BOLD | Font.ITALIC, 40));
		label_7.setBounds(449, 12, 375, 58);
		panelOperaciones.add(label_7);
		
		JLabel lblEntradassalidas = new JLabel("Entradas/Salidas");
		lblEntradassalidas.setForeground(Color.WHITE);
		lblEntradassalidas.setFont(new Font("Dialog", Font.BOLD, 24));
		lblEntradassalidas.setBounds(468, 102, 303, 30);
		panelOperaciones.add(lblEntradassalidas);
		
		JLabel lblTipoDeOperacin = new JLabel("Tipo de Operación:");
		lblTipoDeOperacin.setForeground(Color.WHITE);
		lblTipoDeOperacin.setFont(new Font("Dialog", Font.BOLD, 18));
		lblTipoDeOperacin.setBounds(323, 183, 190, 30);
		panelOperaciones.add(lblTipoDeOperacin);
		
		JLabel lblTipoDePago = new JLabel("Tipo de Pago:");
		lblTipoDePago.setForeground(Color.WHITE);
		lblTipoDePago.setFont(new Font("Dialog", Font.BOLD, 18));
		lblTipoDePago.setBounds(370, 243, 147, 30);
		panelOperaciones.add(lblTipoDePago);
		
		JLabel lblDescripcin = new JLabel("Descripción:");
		lblDescripcin.setForeground(Color.WHITE);
		lblDescripcin.setFont(new Font("Dialog", Font.BOLD, 18));
		lblDescripcin.setBounds(381, 306, 126, 30);
		panelOperaciones.add(lblDescripcin);
		
		cmbAdminOpPayType = new JComboBox<String>();
		cmbAdminOpPayType.setBounds(525, 247, 160, 24);
		panelOperaciones.add(cmbAdminOpPayType);
		cmbAdminOpPayType.addItem("Efectivo");
		cmbAdminOpPayType.addItem("Tarjeta");
		
		cmbOpType = new JComboBox<String>();
		cmbOpType.setBounds(525, 187, 160, 24);
		panelOperaciones.add(cmbOpType);
		cmbOpType.addItem("ENTRADA");
		cmbOpType.addItem("SALIDA");
		
		JLabel lblCantidad_1 = new JLabel("Cantidad: $");
		lblCantidad_1.setForeground(Color.WHITE);
		lblCantidad_1.setFont(new Font("Dialog", Font.BOLD, 18));
		lblCantidad_1.setBounds(403, 374, 118, 30);
		panelOperaciones.add(lblCantidad_1);
		
		txtOpDescription = new JTextField();
		txtOpDescription.setColumns(10);
		txtOpDescription.setBounds(525, 308, 196, 30);
		panelOperaciones.add(txtOpDescription);
		
		txtOpCantidad = new JTextField();
		txtOpCantidad.setColumns(10);
		txtOpCantidad.setBounds(525, 376, 55, 30);
		panelOperaciones.add(txtOpCantidad);
		
		JButton btnOpGenerar = new JButton("Generar Operación");
		btnOpGenerar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generateOperationProcess();
			}
		});
		btnOpGenerar.setForeground(Color.WHITE);
		btnOpGenerar.setBackground(new Color(50, 205, 50));
		btnOpGenerar.setBounds(508, 452, 179, 39);
		panelOperaciones.add(btnOpGenerar);

		
/////////////////////////////////////// INICIA CONSTRUCCION PARA EL TAB DE ADMINISTRACION //////////////////////
		
		JPanel panelAdministration = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(imagenFondo, 0, 0, getWidth(), getHeight(), this);
			}
			};
		panelAdministration.setLayout(null);
		tabbedPane.addTab("Administración", null, panelAdministration, null);
		
		JLabel label_9 = new JLabel("NewYork Coffee ");
		label_9.setForeground(Color.WHITE);
		label_9.setFont(new Font("URW Chancery L", Font.BOLD | Font.ITALIC, 40));
		label_9.setBounds(408, 24, 408, 58);
		panelAdministration.add(label_9);
		
		JLabel label_10 = new JLabel("Alta de Administrador");
		label_10.setForeground(Color.WHITE);
		label_10.setFont(new Font("Dialog", Font.BOLD, 24));
		label_10.setBounds(110, 152, 313, 30);
		panelAdministration.add(label_10);
		
		JLabel lblNombre = new JLabel("Tipo de empleado:");
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setFont(new Font("Dialog", Font.BOLD, 18));
		lblNombre.setBounds(59, 224, 185, 30);
		panelAdministration.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Nombre:");
		lblApellido.setForeground(Color.WHITE);
		lblApellido.setFont(new Font("Dialog", Font.BOLD, 18));
		lblApellido.setBounds(152, 276, 92, 30);
		panelAdministration.add(lblApellido);
		
		txtAdmNombre = new JTextField();
		txtAdmNombre.setColumns(10);
		txtAdmNombre.setBounds(247, 278, 176, 30);
		panelAdministration.add(txtAdmNombre);
		
		JLabel label_6 = new JLabel("Apellido:");
		label_6.setForeground(Color.WHITE);
		label_6.setFont(new Font("Dialog", Font.BOLD, 18));
		label_6.setBounds(152, 332, 92, 30);
		panelAdministration.add(label_6);
		
		txtAdmApellido = new JTextField();
		txtAdmApellido.setColumns(10);
		txtAdmApellido.setBounds(247, 334, 176, 30);
		panelAdministration.add(txtAdmApellido);
		
		JLabel lblContrasea = new JLabel("Contraseña:");
		lblContrasea.setForeground(Color.WHITE);
		lblContrasea.setFont(new Font("Dialog", Font.BOLD, 18));
		lblContrasea.setBounds(119, 432, 125, 30);
		panelAdministration.add(lblContrasea);
		
		JLabel lblRepitaContrasea = new JLabel("Repita Contraseña:");
		lblRepitaContrasea.setForeground(Color.WHITE);
		lblRepitaContrasea.setFont(new Font("Dialog", Font.BOLD, 18));
		lblRepitaContrasea.setBounds(44, 474, 200, 30);
		panelAdministration.add(lblRepitaContrasea);
		
		cmbEmployeeType = new JComboBox<String>();
		cmbEmployeeType.setBounds(247, 228, 160, 24);
		panelAdministration.add(cmbEmployeeType);
		cmbEmployeeType.addItem("Administrador");
		cmbEmployeeType.addItem("Empleado");
		
		txtPassUno = new JPasswordField();
		txtPassUno.setBounds(247, 434, 125, 30);
		panelAdministration.add(txtPassUno);
		
		txtAdmTel = new JTextField();
		txtAdmTel.setColumns(10);
		txtAdmTel.setBounds(247, 380, 176, 30);
		panelAdministration.add(txtAdmTel);
		
		JLabel lblTelefono = new JLabel("Telefono:");
		lblTelefono.setForeground(Color.WHITE);
		lblTelefono.setFont(new Font("Dialog", Font.BOLD, 18));
		lblTelefono.setBounds(147, 378, 97, 30);
		panelAdministration.add(lblTelefono);
		
		txtPassDos = new JPasswordField();
		txtPassDos.setBounds(247, 476, 125, 30);
		panelAdministration.add(txtPassDos);
		
		JLabel lblAdministracin = new JLabel("Administración");
		lblAdministracin.setForeground(Color.WHITE);
		lblAdministracin.setFont(new Font("URW Chancery L", Font.BOLD | Font.ITALIC, 40));
		lblAdministracin.setBounds(418, 81, 327, 44);
		panelAdministration.add(lblAdministracin);
		
		JLabel lblListaDeAdministradores = new JLabel("Lista de Administradores");
		lblListaDeAdministradores.setForeground(Color.WHITE);
		lblListaDeAdministradores.setFont(new Font("Dialog", Font.BOLD, 24));
		lblListaDeAdministradores.setBounds(639, 152, 429, 30);
		panelAdministration.add(lblListaDeAdministradores);
		
		spListAdmin = new JScrollPane();
		spListAdmin.setBounds(568, 196, 530, 297);
		panelAdministration.add(spListAdmin);
		
		tblAdministrators = new JTable();
		spListAdmin.setColumnHeaderView(tblAdministrators);
		
		JButton btnEliminarAdm = new JButton("Eliminar Adm");
		btnEliminarAdm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteAdminProcess();
			}
		});
		btnEliminarAdm.setForeground(Color.WHITE);
		btnEliminarAdm.setBackground(Color.RED);
		btnEliminarAdm.setBounds(962, 525, 155, 37);
		panelAdministration.add(btnEliminarAdm);
		
		JButton btnCambiarPassAdm = new JButton("Cambiar Contraseña");
		btnCambiarPassAdm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateAdminPassProcess();
			}
		});
		btnCambiarPassAdm.setFont(new Font("Dialog", Font.BOLD, 12));
		btnCambiarPassAdm.setForeground(Color.WHITE);
		btnCambiarPassAdm.setBackground(new Color(0, 0, 255));
		btnCambiarPassAdm.setBounds(550, 525, 185, 37);
		panelAdministration.add(btnCambiarPassAdm);
		
		JButton btnMostrarTodosAdm = new JButton("Mostrar Todos");
		btnMostrarTodosAdm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				initAdminListProcess();
			}
		});
		btnMostrarTodosAdm.setForeground(Color.WHITE);
		btnMostrarTodosAdm.setBackground(new Color(255, 165, 0));
		btnMostrarTodosAdm.setBounds(774, 547, 155, 37);
		panelAdministration.add(btnMostrarTodosAdm);
		
		JButton btnGuardarAdm = new JButton("Guardar");
		btnGuardarAdm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveAdminProcess();
			}
		});
		btnGuardarAdm.setForeground(Color.WHITE);
		btnGuardarAdm.setBackground(new Color(50, 205, 50));
		btnGuardarAdm.setBounds(247, 531, 125, 37);
		panelAdministration.add(btnGuardarAdm);
		
		final String adminColumns[] = {"Id","Tipo Adm", "Nombre", "Apellido", "Telefono"};
		tableModelAdmin = new DefaultTableModel(adminColumns, 0);
		tblAdministrators = new JTable(tableModelAdmin);
		tblAdministrators.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tblAdministrators.getColumnModel().getColumn(0).setPreferredWidth(30);
		tblAdministrators.getColumnModel().getColumn(1).setPreferredWidth(130);
		tblAdministrators.getColumnModel().getColumn(2).setPreferredWidth(130);
		tblAdministrators.getColumnModel().getColumn(3).setPreferredWidth(130);
		tblAdministrators.getColumnModel().getColumn(4).setPreferredWidth(130);
		initAdminListProcess();

	}
	

/////////////////////// INICIO DE METODOS QUE SON LLAMADOS DESDE LOS EVENTOS DE LOS BOTONES Y LOS KEY RELEASES ///////////
	
	/**
	 * executeSaleProcess executes the sale on process to be stored at data base.
	 */
	public void initCombosProcess() {
		cmbHotDrinksList.removeAllItems();
		hotDrinkList = null;
		hotDrinkList = ProductClient.getHotDrinks();
		for (Products hotProducts : hotDrinkList) {
			cmbHotDrinksList.addItem(hotProducts.getProductName()+" "+hotProducts.getSize()+"     $"+hotProducts.getPrice().toString());
		}
		
		cmbColdDrinksList.removeAllItems();
		coldDrinkList = null;
		coldDrinkList = ProductClient.getColdDrinks();
		for (Products coldProducts : coldDrinkList) {
			cmbColdDrinksList.addItem(coldProducts.getProductName()+" "+coldProducts.getSize()+"     $"+coldProducts.getPrice().toString());
		}
		
		cmbFoodList.removeAllItems();
		foodList = null;
		foodList = ProductClient.getFood();
		for (Products foodProducts : foodList) {
			cmbFoodList.addItem(foodProducts.getProductName()+"     $"+foodProducts.getPrice().toString());
		}
		
		cmbComplementoBebidas.removeAllItems();
		drinkComplementList = null;
		drinkComplementList = ProductClient.getDrinkComplement();
		for (Products drinkComplement : drinkComplementList) {
			cmbComplementoBebidas.addItem(drinkComplement.getProductName()+"     $"+drinkComplement.getPrice().toString());
		}
		
		cmbComplementoComidas.removeAllItems();
		foodComplementList = null;
		foodComplementList = ProductClient.getFoodComplement();
		for (Products foodComplement : foodComplementList) {
			cmbComplementoComidas.addItem(foodComplement.getProductName()+"     $"+foodComplement.getPrice().toString());
		}
		
	}
	
	/**
	 * executeSaleProcess executes the sale on process to be stored at data base.
	 */
	public void initBalanceProcess() {
		Incomes[] incomes;
		incomes = IncomesClient.getAllIncomes();
		tableModelBalanceIn.setRowCount(0);
		txtBalTotalIn.setText(null);
		for (Incomes ins : incomes) {
			Object[] incomeItems = { ins.getIdIncome(), ins.getNoSale(),ins.getDescription(), ins.getPaymentType(), 
					ins.getTotal(),ins.getIncomeDate(), ins.getEmployee()};
			tableModelBalanceIn.addRow(incomeItems);
		}
		txtBalTotalIn.setText(BalanceCalculating.getTotalIncomes(incomes));
		spBalEntradas.setViewportView(tableBalanceIn);
		
		Outgoings[] outgoings;
		outgoings = OutgoingsClient.getAllOutGoings();
		tableModelBalanceOut.setRowCount(0);
		txtBalTotalOut.setText(null);
		for (Outgoings outs : outgoings) {
			Object[] outgoingsItems = { outs.getIdOutgoing(), outs.getDescription(), outs.getPaymentType(),
					outs.getTotal(), outs.getOutgoingDate(), outs.getEmployee()};
			tableModelBalanceOut.addRow(outgoingsItems);
		}
		txtBalTotalOut.setText(BalanceCalculating.getTotalOutGoings(outgoings));
		spBalSalidas.setViewportView(tableBalanceOut);
		
	}
	
	/**
	 * executeSaleProcess executes the sale on process to be stored at data base.
	 * @throws InterruptedException 
	 * @throws PrinterException 
	 */
	public void executeSaleProcess() throws PrinterException, InterruptedException {
		int row;
		Cash cash = new Cash();
		int saleStatus = 200;
		int incomeStatus = 200;
		String description;
		String paymentType;
		String clientName;
		boolean creditCard=false;
		double total;
		double totalSale;
		double cambio;
		double received;
		String saleDate;
		SaleTicketModel saleTicketModel = new SaleTicketModel();
		try {
			paymentType = (String) cmbSalePayType.getSelectedItem();
			saleDate = TempSaleCalculating.getCurrentTimeUsingDate();
			totalSale = Double.parseDouble(txtSaleTotal.getText());
			cambio = Double.parseDouble(txtSaleChange.getText());
			received = Double.parseDouble(txtSaleReceived.getText());
			clientName = txtClientName.getText();
			saleTicketModel.setEmployee(employee);
			saleTicketModel.setNoSale(String.valueOf(noVenta));
			saleTicketModel.setPayType(paymentType);
			saleTicketModel.setTotal(String.valueOf(totalSale));
			saleTicketModel.setReceived(String.valueOf(received));
			saleTicketModel.setChange(String.valueOf(cambio));
			saleTicketModel.setSaleDate(saleDate);
			saleTicketModel.setTempSaleList(tempSalePrint);
			row = tblTempSaleList.getRowCount();
			if(paymentType.equals(PaymentTypeCatalog.CARD)) {
				creditCard=true;
			}
			if (cashQuantity > cambio && row > 0) {
				if (cambio >= 0.0 && received > 0.0) {
					if (!txtSaleTotal.getText().isEmpty() || !txtSaleReceived.getText().isEmpty()) {
						row = tblTempSaleList.getRowCount();
						for (int i = 0; i < row; i++) {
							Sales sale = new Sales();
							description = (String) tblTempSaleList.getValueAt(i, 1);
							total = (Double) tblTempSaleList.getValueAt(i, 3);
							sale.setNoSale(noVenta);
							sale.setDescription(description);
							sale.setPaymentType(paymentType);
							sale.setTotal(total);
							sale.setSaleDate(saleDate);
							saleStatus = SalesClient.addSale(sale);
							if (saleStatus != 200) {
								JOptionPane.showMessageDialog(null,
										"El servicio de ventas tuvo un problema, reintente por favor!");
								break;
							}
						}
						if (saleStatus == 200) {
							Incomes income = new Incomes();
							income.setNoSale(noVenta);
							income.setDescription(PaymentTypeCatalog.SALE_DESCRIPTION);
							income.setPaymentType(paymentType);
							income.setTotal(totalSale);
							income.setIncomeDate(saleDate);
							income.setEmployee(employee);
							incomeStatus = IncomesClient.addIncome(income);
							if (incomeStatus != 200) {
								JOptionPane.showMessageDialog(null,
										"Ocurrio un problema al ingresar la venta, Favor de ingresarla manualmente en Entradas porfavor!","WARNING_MESSAGE", JOptionPane.ERROR_MESSAGE);
							}
							JOptionPane.showMessageDialog(null, "Venta Realizada!");
							// INICIA PROCESO DE IMPRESION //
							printSaleProcess(saleTicketModel,clientName);
							// Limpiando tabla de venta y reiniciando campos
							tableModelTempSaleList.setRowCount(0);
							TemporarySaleClient.truncateTempSale();
							cash = TempSaleCalculating.updateCashProcess(totalSale,creditCard);
							noVenta = cash.getNoSale();
							cashQuantity = cash.getQuantity();
							txtSaleTotal.setText("0.0");
							txtSaleReceived.setText("0.0");
							txtSaleChange.setText("0.0");
							txtDrinksQuantity.setText("1");
							txtJunkQuantity.setText("1");
							txtClientName.setText("XXX");
							chckBebidaCaliente.setSelected(false);
							chckBebidaFria.setSelected(false);
							chckAgregarCompBebida.setSelected(false);
							chckAgregarCompComida.setSelected(false);
							cmbSalePayType.setSelectedIndex(0);
							tempSalePrint=null;
							initBalanceProcess();
							initListSaleCheckProcess();
							JOptionPane.showMessageDialog(null, "Cajon Actualizado: " + cashQuantity);
							if(cashQuantity>PaymentTypeCatalog.LIMIT_CASH) {
							JOptionPane.showMessageDialog(null, "Hay excedente en cajón, favor de enviar dinero a Administración!");	
							}
						}
					} else {
						JOptionPane.showMessageDialog(null, "Verifique el total y el recibido por favor!");
					}
				} else {
					String message = cambio < 0.0 ? "Verifique el cambio por favor!"
							: "Ingrese dinero recibido por favor!";
					JOptionPane.showMessageDialog(null, message,"WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
				}
			} else {
				String message = row < 1 ? "No hay productos en la lista de venta"
						: "No hay disponible en caja para dar cambio, ingrese dinero por favor!";
				JOptionPane.showMessageDialog(null, message,"WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
			}
		} catch (NumberFormatException nf) {
			System.out.println("Number Format not valid, no letters allowed at Total field");
		}
	}

	/**
	 * cleanTempTableProcess executes the clean up process for the tempSale table.
	 */
	public void cleanTempTableProcess() {
		TemporarySaleClient.truncateTempSale();
		tableModelTempSaleList.setRowCount(0);
		txtSaleTotal.setText("0.0");
		txtSaleReceived.setText("0.0");
		txtSaleChange.setText("0.0");
		txtDrinksQuantity.setText("1");
		txtJunkQuantity.setText("1");
		txtClientName.setText("-");
		chckBebidaCaliente.setSelected(false);
		chckBebidaFria.setSelected(false);
		chckAgregarCompBebida.setSelected(false);
		chckAgregarCompComida.setSelected(false);
	}
	
	/**
	 * removeSelectedSaleProcess executes the removal process for the tempSale table.
	 */
	public void removeSelectedSaleProcess() {
		int row;
		int idTempSale;
		row = tblTempSaleList.getSelectedRow();
		if (row > -1) {
			idTempSale = (Integer) tblTempSaleList.getValueAt(row, 0);
			TemporarySaleClient.deleteTempSale(idTempSale);
			tableModelTempSaleList.setRowCount(0);

			TemporarySale[] tempSale;
			tempSale = TemporarySaleClient.getAllTempSales();
			if (tempSale.length == 0) {
				TemporarySaleClient.truncateTempSale();
				tableModelTempSaleList.setRowCount(0);
				txtSaleTotal.setText("0.0");
				txtSaleReceived.setText("0.0");
				txtSaleChange.setText("0.0");
				txtDrinksQuantity.setText("1");
				txtJunkQuantity.setText("1");
				txtClientName.setText("-");
				chckBebidaCaliente.setSelected(false);
				chckBebidaFria.setSelected(false);
				chckAgregarCompBebida.setSelected(false);
				chckAgregarCompComida.setSelected(false);
			} else {
				for (TemporarySale temporarySale : tempSale) {
					Object[] saleItems = { temporarySale.getIdTempSale(), temporarySale.getDescription(),
							temporarySale.getQuantity(), temporarySale.getTotal() };
					tableModelTempSaleList.addRow(saleItems);
				}
				txtSaleTotal.setText(TempSaleCalculating.getTotalTempSale(tempSale));
				scrollPaneSale.setViewportView(tblTempSaleList);
			}
		} else {
			JOptionPane.showMessageDialog(null, "Seleccione un producto a eliminar por favor","WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * addFoodProcess executes add food process for the tempSale table.
	 */
	public void addFoodProcess() {
		TemporarySale tempSale = new TemporarySale();
		double total = 0.0;
		double totalComp = 0.0;
		String foodDescription = null;
		String complDesc = null;
		String finalDescription = null;
		Integer quantity = 1;
		tempSalePrint = null;
		boolean proceed = true;
		try {
		if (txtJunkQuantity.getText().equals("0") || txtJunkQuantity.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Ingrese cantidad de Aperitivos!");
			proceed = false;
		}
		if (!chckAgregarCompComida.isSelected() && proceed) {
			quantity = Integer.parseInt(txtJunkQuantity.getText());
			total = TempSaleCalculating.getTotalFromQuantity(quantity,
					TempSaleCalculating.getProductComboPrice((String) cmbFoodList.getSelectedItem()));
			finalDescription = TempSaleCalculating
					.getProductComboDescription((String) cmbFoodList.getSelectedItem());
		} else if (chckAgregarCompComida.isSelected() && proceed) {
			quantity = Integer.parseInt(txtJunkQuantity.getText());
			totalComp = TempSaleCalculating.getTotalFromQuantity(quantity,
					TempSaleCalculating.getProductComboPrice((String) cmbFoodList.getSelectedItem()));
			total = TempSaleCalculating.getTotalWithComplement(quantity,
					TempSaleCalculating.getProductComboPrice((String) cmbComplementoComidas.getSelectedItem()),
					totalComp);
			foodDescription = TempSaleCalculating
					.getProductComboDescription((String) cmbFoodList.getSelectedItem());
			complDesc = TempSaleCalculating
					.getProductComboDescription((String) cmbComplementoComidas.getSelectedItem());
			finalDescription = foodDescription + "+" + complDesc;
		}
		if (proceed) {
			tempSale.setDescription(finalDescription);
			tempSale.setQuantity(quantity);
			tempSale.setTotal(total);
			TemporarySaleClient.addTempSale(tempSale);

			TemporarySale[] tempSaleAdd;
			tempSaleAdd = TemporarySaleClient.getAllTempSales();
			tableModelTempSaleList.setRowCount(0);
			for (TemporarySale temporarySaleTable : tempSaleAdd) {
				Object[] saleItems = { temporarySaleTable.getIdTempSale(), temporarySaleTable.getDescription(),
						temporarySaleTable.getQuantity(), temporarySaleTable.getTotal() };
				tableModelTempSaleList.addRow(saleItems);
			}
			tempSalePrint = tempSaleAdd;
			txtSaleTotal.setText(TempSaleCalculating.getTotalTempSale(tempSaleAdd));
			txtSaleReceived.setText("0.0");
			txtSaleChange.setText("0.0");
		}
		}catch(NumberFormatException nf) {
			JOptionPane.showMessageDialog(null,"Favor de agregar numero en la cantidad de aperitivos por favor!","WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * addFoodProcess executes add drink process for the tempSale table.
	 */
	public void addDrinkProcess() {
		TemporarySale tempSale = new TemporarySale();
		double total = 0.0;
		double totalComp = 0.0;
		String drinkDescription = null;
		String complDesc = null;
		String finalDescription = null;
		Integer quantity = 1;
		tempSalePrint = null;
		boolean proceed = true;
		try {
		if (txtDrinksQuantity.getText().equals("0") || txtDrinksQuantity.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Ingrese cantidad de bebidas!");
			proceed = false;
		} else if ((chckBebidaCaliente.isSelected() && chckBebidaFria.isSelected())
				|| (!chckBebidaCaliente.isSelected() && !chckBebidaFria.isSelected())) {
			JOptionPane.showMessageDialog(null, "Seleccione solo 1 tipo de bebida!");
			proceed = false;
		} else if (chckBebidaCaliente.isSelected()) {
			if (!chckAgregarCompBebida.isSelected()) {
				quantity = Integer.parseInt(txtDrinksQuantity.getText());
				total = TempSaleCalculating.getTotalFromQuantity(quantity,
						TempSaleCalculating.getProductComboPrice((String) cmbHotDrinksList.getSelectedItem()));
				finalDescription = TempSaleCalculating
						.getProductComboDescription((String) cmbHotDrinksList.getSelectedItem());
			} else {
				quantity = Integer.parseInt(txtDrinksQuantity.getText());
				totalComp = TempSaleCalculating.getTotalFromQuantity(quantity,
						TempSaleCalculating.getProductComboPrice((String) cmbHotDrinksList.getSelectedItem()));
				total = TempSaleCalculating.getTotalWithComplement(quantity,TempSaleCalculating
						.getProductComboPrice((String) cmbComplementoBebidas.getSelectedItem()), totalComp);
				drinkDescription = TempSaleCalculating
						.getProductComboDescription((String) cmbHotDrinksList.getSelectedItem());
				complDesc = TempSaleCalculating
						.getProductComboDescription((String) cmbComplementoBebidas.getSelectedItem());
				finalDescription = drinkDescription + "+" + complDesc;
			}
		} else if (chckBebidaFria.isSelected()) {
			if (!chckAgregarCompBebida.isSelected()) {
				quantity = Integer.parseInt(txtDrinksQuantity.getText());
				total = TempSaleCalculating.getTotalFromQuantity(quantity,
						TempSaleCalculating.getProductComboPrice((String) cmbColdDrinksList.getSelectedItem()));
				finalDescription = TempSaleCalculating
						.getProductComboDescription((String) cmbColdDrinksList.getSelectedItem());
			} else {
				quantity = Integer.parseInt(txtDrinksQuantity.getText());
				totalComp = TempSaleCalculating.getTotalFromQuantity(quantity,
						TempSaleCalculating.getProductComboPrice((String) cmbColdDrinksList.getSelectedItem()));
				total = TempSaleCalculating.getTotalWithComplement(quantity, TempSaleCalculating
						.getProductComboPrice((String) cmbComplementoBebidas.getSelectedItem()), totalComp);
				drinkDescription = TempSaleCalculating
						.getProductComboDescription((String) cmbColdDrinksList.getSelectedItem());
				complDesc = TempSaleCalculating
						.getProductComboDescription((String) cmbComplementoBebidas.getSelectedItem());
				finalDescription = drinkDescription + "+" + complDesc;
			}
		}
		if (proceed) {
			tempSale.setDescription(finalDescription);
			tempSale.setQuantity(quantity);
			tempSale.setTotal(total);
			TemporarySaleClient.addTempSale(tempSale);
			tableModelTempSaleList.setRowCount(0);
			TemporarySale[] tempSaleAdd;
			tempSaleAdd = TemporarySaleClient.getAllTempSales();

			for (TemporarySale temporarySaleTable : tempSaleAdd) {
				Object[] saleItems = { temporarySaleTable.getIdTempSale(), temporarySaleTable.getDescription(),
						temporarySaleTable.getQuantity(), temporarySaleTable.getTotal() };
				tableModelTempSaleList.addRow(saleItems);
			}
			tempSalePrint = tempSaleAdd;
			txtSaleTotal.setText(TempSaleCalculating.getTotalTempSale(tempSaleAdd));
			txtSaleReceived.setText("0.0");
			txtSaleChange.setText("0.0");
		}
		
	}catch(NumberFormatException nf) {
		JOptionPane.showMessageDialog(null,"Favor de agregar numero en la cantidad de bebidas por favor!","WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
	}
	}
	
	/**
	 * saleReceivedProcess executes the calculation for the change.
	 */
	public void saleReceivedProcess() {
		String change;
		if(txtSaleReceived.getText().isEmpty()) {
			txtSaleChange.setText("0.00");
		}
		else {
			change =TempSaleCalculating.getChangeSale(txtSaleTotal.getText(), txtSaleReceived.getText());
			txtSaleChange.setText(change);
		}
	}
	
	/**
	 * drinkQuantityProcess executes the quantity calculation for drinks.
	 */
	public void drinkQuantityProcess() {
		try {
		double drinkNumber;
		if(txtDrinksQuantity.getText().isEmpty()) {
			System.out.println("Waiting for number to be typed");
		}else {
		drinkNumber = Double.parseDouble(txtDrinksQuantity.getText());
		}
		}catch(NumberFormatException nf) {
			System.out.println("Number Format not valid, no letters allowed at Quantity field");
		}
	}
	
	/**
	 * junkQuantityProcess executes the quantity calculation for food.
	 */
	public void junkQuantityProcess() {
		try {
			double junkNumber;
			if(txtJunkQuantity.getText().isEmpty()) {
				System.out.println("Waiting for number to be typed");
			}else {
				junkNumber = Double.parseDouble(txtJunkQuantity.getText());
			}
			}catch(NumberFormatException nf) {
				System.out.println("Number Format not valid, no letters allowed at Quantity field");
			}
	}
	
	/**
	 * getCashInfoProcess shows the cash quantity to the user.
	 */
	public void getCashInfoProcess() {
		double cashInfo;
		cashInfo = TempSaleCalculating.getCashQuantity();
		JOptionPane.showMessageDialog(null,"Cantidad en caja: "+cashInfo,"NewYork Coffee", JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * generateBalanceProcess shows the balance quantity to the user.
	 */
	public void generateBalanceProcess(boolean showBalance) {
		double balance;
		double initCash;
		String qIncome;
		String qOutgoings;
		tableModelBalanceIn.setRowCount(0);
		tableModelBalanceOut.setRowCount(0);
		
		Incomes[] incomes;
		incomes = IncomesClient.getAllIncomes();
		for (Incomes ins : incomes) {
			Object[] incomeItems = { ins.getIdIncome(), ins.getNoSale(),ins.getDescription(), ins.getPaymentType(), 
					ins.getTotal(),ins.getIncomeDate(), ins.getEmployee()};
			tableModelBalanceIn.addRow(incomeItems);
		}
		qIncome = BalanceCalculating.getTotalIncomes(incomes);
		txtBalTotalIn.setText(qIncome);
		initCash = (Double) tableBalanceIn.getValueAt(0, 4);
		
		Outgoings[] outgoings;
		outgoings = OutgoingsClient.getAllOutGoings();
		for (Outgoings outs : outgoings) {
			Object[] outgoingsItems = { outs.getIdOutgoing(), outs.getDescription(), outs.getPaymentType(),
					outs.getTotal(), outs.getOutgoingDate()};
			tableModelBalanceOut.addRow(outgoingsItems);
		}
		qOutgoings = BalanceCalculating.getTotalOutGoings(outgoings);
		txtBalTotalOut.setText(qOutgoings);
		balance = BalanceCalculating.getBalance(qIncome, qOutgoings, initCash);
		if(balance >=1 && showBalance) {
			JOptionPane.showMessageDialog(null,"Ganancias Generadas por: "+balance,"NewYork Coffee", JOptionPane.INFORMATION_MESSAGE);
		}else if(balance <1 && showBalance){
			JOptionPane.showMessageDialog(null,"Pérdidas Generadas por: "+balance,"NewYork Coffee", JOptionPane.INFORMATION_MESSAGE);
			JOptionPane.showMessageDialog(null,"Favor de revisar movimientos y realizar adición si es necesario","NewYork Coffee", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	/**
	 * showAllProducts for the new product tab section.
	 */
	public void showAllProductsProcess() {
		final String newProdColumns[] = { "Id", "Producto", "Tipo Producto", "Tamaño","Precio" };
		tableModelAltaProd = new DefaultTableModel(newProdColumns, 0);
		tblAltaProductos = new JTable(tableModelAltaProd);
		tblAltaProductos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tblAltaProductos.getColumnModel().getColumn(0).setPreferredWidth(30);
		tblAltaProductos.getColumnModel().getColumn(1).setPreferredWidth(210);
		tblAltaProductos.getColumnModel().getColumn(2).setPreferredWidth(180);
		tblAltaProductos.getColumnModel().getColumn(3).setPreferredWidth(100);
		tblAltaProductos.getColumnModel().getColumn(3).setPreferredWidth(100);
		Products[] products;
		products = ProductClient.getAllProducts();
		for (Products prods : products) {
			Object[] prodItems = { prods.getIdProduct(), prods.getProductName(),
					prods.getProductType(), prods.getSize(),prods.getPrice() };
			tableModelAltaProd.addRow(prodItems);
		}
		scrollPanepAltaProductos.setViewportView(tblAltaProductos);
	}
	
	/**
	 * addNewProductProcess for the new product tab section.
	 */
	public void addNewProductProcess() {
		Products product = new Products();
		String nombreProd;
		String tipoProd;
		String sizeProd;
		String price;
		int status;
		double priceProd;
		if(txtAltaNombProd.getText().isEmpty() || txtAltaPrecioProd.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null,"Favor de completar el formulario de alta de productos","Alta Productos", JOptionPane.INFORMATION_MESSAGE);
		}else {
	    try {
		nombreProd = txtAltaNombProd.getText();
		tipoProd = (String)cmbAltaTipoProd.getSelectedItem();
		sizeProd = (String)cmbAltaSizeProd.getSelectedItem();
		price = txtAltaPrecioProd.getText();
		priceProd = Double.parseDouble(price);
		product.setProductName(nombreProd);
		product.setProductType(tipoProd);
		product.setSize(sizeProd);
		product.setPrice(priceProd);
		status = ProductClient.addProduct(product);
		if(status==200) {
			JOptionPane.showMessageDialog(null,"Alta exitosa de Producto!","Alta Productos", JOptionPane.INFORMATION_MESSAGE);
			txtAltaNombProd.setText("");
			txtAltaPrecioProd.setText("");
			showAllProductsProcess();
			initCombosProcess();
		}
		else {
			JOptionPane.showMessageDialog(null,"Problemas al dar de alta, estatus de respuesta: "+status,"Alta Productos", JOptionPane.INFORMATION_MESSAGE);
		}
	    }catch(NumberFormatException nfe) {
	    	System.out.println("Incorrect Price Value");
	    	JOptionPane.showMessageDialog(null,"Favor de colocar número en el campo precio","Alta Productos", JOptionPane.INFORMATION_MESSAGE);
	    }
		}
	}
	
	/**
	 * removeSelectedSaleProcess executes the removal process for the priduct table.
	 */
	public void removeSelectedProductProcess() {
		int row;
		int idProd;
		int status;
		row = tblAltaProductos.getSelectedRow();
		try {
		if (row > -1) {
			idProd = (Integer) tblAltaProductos.getValueAt(row, 0);
			status = ProductClient.deleteProduct(idProd);
			if(status==200) {
			JOptionPane.showMessageDialog(null,"Producto Borrado!","Alta Productos", JOptionPane.INFORMATION_MESSAGE);
			tableModelAltaProd.setRowCount(0);
			showAllProductsProcess();
			initCombosProcess();
			}else {
				JOptionPane.showMessageDialog(null,"Problemas al borrar producto, estatus de respuesta: "+status,"Alta Productos", JOptionPane.INFORMATION_MESSAGE);	
			}
			}
		else {
			JOptionPane.showMessageDialog(null, "Seleccione un producto a eliminar por favor","WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
		}
		}catch(NumberFormatException nfe) {
			System.out.println("Id debe ser numerico");
			JOptionPane.showMessageDialog(null, "El Id del Producto no es válido, verifique!","WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * updateSelectedProductProcess executes the update process for the product table.
	 */
	public void updateSelectedProductProcess() {
		int row;
		int idProduct;
		int status;
		String productName;
		String productType;
		String size;
		double price;
		Products product = new Products();
		row = tblAltaProductos.getSelectedRow();
		try {
		if (row > -1) {
			idProduct = (Integer) tblAltaProductos.getValueAt(row, 0);
			productName = (String) tblAltaProductos.getValueAt(row, 1);
			productType = (String) tblAltaProductos.getValueAt(row, 2);
			size = (String) tblAltaProductos.getValueAt(row, 3);
			price = (Double) tblAltaProductos.getValueAt(row, 4);
			product.setIdProduct(idProduct);
			product.setProductName(productName);
			product.setProductType(productType);
			product.setSize(size);
			product.setPrice(price);
			System.out.println(product.toString());
			status = ProductClient.updateProduct(product);
			if(status==200) {
			JOptionPane.showMessageDialog(null,"Producto Actualizado!","Alta Productos", JOptionPane.INFORMATION_MESSAGE);
			tableModelAltaProd.setRowCount(0);
			showAllProductsProcess();
			initCombosProcess();
			}else {
				JOptionPane.showMessageDialog(null,"Problemas al actualizar producto, estatus de respuesta: "+status,"Alta Productos", JOptionPane.INFORMATION_MESSAGE);	
			}
			}
		else {
			JOptionPane.showMessageDialog(null, "Seleccione un producto a eliminar por favor","WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
		}
		}catch(NumberFormatException nfe) {
			System.out.println("Id debe ser numerico");
			JOptionPane.showMessageDialog(null, "El Id del Producto no es válido, verifique!","WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * executeSaleProcess executes the sale initiaizer process to show.
	 */
	public void initListSaleCheckProcess() {
		Sales[] sales;
		sales = SalesClient.getAllSales();
		tableModelShowSaleList.setRowCount(0);
		txtTotalListaVenta.setText(null);
		for (Sales sa : sales) {
			Object[] salesItems = {sa.getNoSale(), sa.getDescription(),sa.getPaymentType(), sa.getTotal(), sa.getSaleDate()};
			tableModelShowSaleList.addRow(salesItems);
		}
		txtTotalListaVenta.setText(BalanceCalculating.getTotalSaleList(sales));
		spListSaleCheck.setViewportView(tblShowSaleList);	
	}
	
	/**
	 * searchNoSaleProcess executes the sale search process to be show at list table.
	 */
	public void searchNoSaleProcess(String noSale) {
		int numberSale;
		try {
		numberSale= Integer.parseInt(noSale);
		Sales[] sales;
		sales = SalesClient.getSaleByNoSale(numberSale);
		tableModelShowSaleList.setRowCount(0);
		txtTotalListaVenta.setText(null);
		for (Sales sa : sales) {
			Object[] salesItems = {sa.getNoSale(), sa.getDescription(),sa.getPaymentType(), sa.getTotal(), sa.getSaleDate()};
			tableModelShowSaleList.addRow(salesItems);
		}
		txtTotalListaVenta.setText(BalanceCalculating.getTotalSaleList(sales));
		spListSaleCheck.setViewportView(tblShowSaleList);	
		}catch(NumberFormatException nfe) {
			tableModelShowSaleList.setRowCount(0);
			txtTotalListaVenta.setText(null);
		System.out.println("Invalid Number, characters are not allowed at this field!");
		}
	}
	
	/**
	 * saleCancellationProcess executes the cancellation sale on process to be removed at data base.
	 */
	public void saleCancellationProcess(String noSale) {
		int numberSale;
		int statusDelete=0;
		int statusOutgoing=0;
		String description= PaymentTypeCatalog.CANCEL_SALE_DESCRIPTION+noSale;
		String paymentType;
		String saleDate;
		double totalSale;
		Cash cash = new Cash();
		boolean cancellation = true;
		boolean creditCard = false;
		int row = tblShowSaleList.getRowCount();
		try {
			if(row > 0 && !txtNoListaVenta.getText().isEmpty()) {
			Outgoings out = new Outgoings();
			paymentType=(String) tblShowSaleList.getValueAt(0, 2);
			totalSale= Double.parseDouble(txtTotalListaVenta.getText());
			saleDate=(String) tblShowSaleList.getValueAt(0, 4);
			out.setDescription(description);		
			out.setPaymentType(paymentType);
			out.setTotal(totalSale);
			out.setOutgoingDate(saleDate);
			out.setEmployee(employee);
			if(paymentType.equals(PaymentTypeCatalog.CARD)) {
				creditCard=true;
			}
			statusOutgoing=OutgoingsClient.addOutGoing(out);
			////ANTES SE REALIZA LA SALIDA EN OUTGOINGS////
			if(statusOutgoing==200) {
					numberSale= Integer.parseInt(noSale);
					statusDelete = SalesClient.deleteByNoSale(numberSale);
					if(statusDelete==200) {	
						cash = TempSaleCalculating.updateCashProcess(totalSale,creditCard,cancellation);
						noVenta = cash.getNoSale();
						cashQuantity = cash.getQuantity();
						tableModelShowSaleList.setRowCount(0);
						txtTotalListaVenta.setText(null);
						txtNoListaVenta.setText(null);
						initListSaleCheckProcess();
						initBalanceProcess();
						JOptionPane.showMessageDialog(null, "Cancelación de venta Realizada. Revise las salidas!","WARNING_MESSAGE", JOptionPane.WARNING_MESSAGE);
					}else {
						JOptionPane.showMessageDialog(null, "Hubo un problema al intentar borrar venta!","INTERNAL ERROR", JOptionPane.ERROR_MESSAGE);	
					}
			}else {
				JOptionPane.showMessageDialog(null, "Hubo un problema al intentar generar salida de dinero!","INTERNAL ERROR", JOptionPane.ERROR_MESSAGE);	
			}
		}else {
			JOptionPane.showMessageDialog(null, "No hay ventas a cancelar en la lista!","WARNING MESSAGE", JOptionPane.WARNING_MESSAGE);	
		}
		}catch(NumberFormatException nfe) {
		System.out.println("Invalid Number, characters are not allowed at this field!");
		}
	}
	
	/**
	 * calculateClosureProcess executes the closure calculus process to show before closing system.
	 */
	public void calculateClosureProcess() {
		Incomes income = new Incomes();
		double totalInCash,totalInCard,totalInCashCard,totalOutCash,totalOutCard,totalOutCashCard,
		totalGenerated,currentCash,initCash;
		
		income = IncomesClient.getIncomeById(PaymentTypeCatalog.ID_CASH);
		initCash = income.getTotal();
		currentCash= TempSaleCalculating.getCashQuantity();
		totalInCash = IncomesClient.getAllIncomesCash();
		totalInCard = IncomesClient.getAllIncomesCards();
		totalInCashCard = totalInCash+totalInCard;
		totalOutCash = OutgoingsClient.getAllOutGoingsCash();
		totalOutCard = OutgoingsClient.getAllOutGoingsCard();
		totalOutCashCard = totalOutCash+totalOutCard;
		totalGenerated = totalInCashCard-totalOutCashCard;
		
		txtCorteInicioCaja.setText(String.valueOf(initCash));
		txtCorteTotEfec.setText(String.valueOf(totalInCash));
		txtCorteTotTarjeta.setText(String.valueOf(totalInCard));
		txtCorteTotEfeTar.setText(String.valueOf(totalInCashCard));
		
		txtCorteSalCaja.setText(String.valueOf(currentCash));
		txtCorteSalEfec.setText(String.valueOf(totalOutCash));
		txtCorteSalTar.setText(String.valueOf(totalOutCard));
		txtCorteSalEfeTar.setText(String.valueOf(totalOutCashCard));
		
		txtCorteTotGene.setText(String.valueOf(totalGenerated));
		
		
	}
	
	/**
	 * cleanClosureProcess cleans the closure calculus process.
	 */
	public void cleanClosureProcess() {
		txtCorteInicioCaja.setText(null);
		txtCorteTotEfec.setText(null);
		txtCorteTotTarjeta.setText(null);
		txtCorteTotEfeTar.setText(null);
		txtCorteSalCaja.setText(null);
		txtCorteSalEfec.setText(null);
		txtCorteSalTar.setText(null);
		txtCorteSalEfeTar.setText(null);
		txtCorteTotGene.setText(null);
	}
	
	/**
	 * closureGenerationProcess generates the closure process and close the system after processed.
	 * @throws Exception 
	 */
	public void closureGenerationProcess() throws Exception {
		Closure closure = new Closure();
		boolean mailProcess=true;
		double totalInCash,totalInCard,totalInCashCard,totalOutCashCard,totalGenerated,initCash;
		String closureDate;
		int statusClosure,statusIncomes,statusOutgoings,statusSales;
		boolean cashOk;
		if(txtCorteInicioCaja.getText().isEmpty() && txtCorteTotEfeTar.getText().isEmpty() 
				&& txtCorteSalEfeTar.getText().isEmpty() && txtCorteTotEfec.getText().isEmpty() 
				&& txtCorteTotTarjeta.getText().isEmpty() && txtCorteTotGene.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Genere el cálculo del corte antes de cerrar!","WARNING MESSAGE", JOptionPane.WARNING_MESSAGE);
		}else {
			try {
			closureDate = TempSaleCalculating.getCurrentTimeUsingDate();
			initCash = Double.parseDouble(txtCorteInicioCaja.getText());
			totalInCashCard = Double.parseDouble(txtCorteTotEfeTar.getText());
			totalOutCashCard = Double.parseDouble(txtCorteSalEfeTar.getText());
			totalInCash = Double.parseDouble(txtCorteTotEfec.getText());
			totalInCard = Double.parseDouble(txtCorteTotTarjeta.getText());		
			totalGenerated = Double.parseDouble(txtCorteTotGene.getText());	
			
			closure.setEmployeeName(employee);
			closure.setInitCash(initCash);
			closure.setTotalIn(totalInCashCard);
			closure.setTotalOut(totalOutCashCard);
			closure.setTotalCash(totalInCash);
			closure.setTotalCard(totalInCard);
			closure.setTotalGenerated(totalGenerated);
			closure.setClosureDate(closureDate);
			statusClosure = ClosureClient.addClosure(closure);
			if(statusClosure==200) {
				statusIncomes = IncomesClient.truncateIncomes();
				statusOutgoings = OutgoingsClient.truncateOutgoings();
				statusSales = SalesClient.truncateSales();
				if(statusIncomes==200 && statusOutgoings==200 && statusSales==200) {
					Cash rebootCash = new Cash();
					rebootCash.setIdCash(PaymentTypeCatalog.ID_CASH);
					rebootCash.setNoSale(PaymentTypeCatalog.ID_CASH);
					rebootCash.setQuantity(0.0);
					cashOk = CashClient.updateCash(rebootCash);
					if(cashOk) {
						//METODO DE IMPRESION DE TICKET
						ClosureTicket.closurePrintService(closure);
						//PROCESO DE ENVIO DE CORREO
						mailProcess = SendClosureMail.SendMail(closure);
						if(!mailProcess) {
							JOptionPane.showMessageDialog(null, "Conexion a Internet Inaccesible, omitiendo envio de corte a administrador.","NEW YORK COFFEE CLOSURE", JOptionPane.INFORMATION_MESSAGE);	
						}
						JOptionPane.showMessageDialog(null, "Cajon Cerrado Correctamente","NEW YORK COFFEE CLOSURE", JOptionPane.INFORMATION_MESSAGE);	
						System.exit(0);
					}else {
						JOptionPane.showMessageDialog(null, "Problema al reiniciar tabla CASH","WARNING MESSAGE", JOptionPane.WARNING_MESSAGE);
					}
				}else {
					JOptionPane.showMessageDialog(null, "Problema al reiniciar tablas INCOMES/OUTGOINGS/SALES","WARNING MESSAGE", JOptionPane.WARNING_MESSAGE);
				}
				
			}else {
				JOptionPane.showMessageDialog(null, "Problema al Cerrar Caja","WARNING MESSAGE", JOptionPane.WARNING_MESSAGE);
			}
			
			}catch(NumberFormatException nfe) {
				JOptionPane.showMessageDialog(null, "Numero Inválido en uno de los campos de corte","WARNING MESSAGE", JOptionPane.WARNING_MESSAGE);
				JOptionPane.showMessageDialog(null, "Limpie y genere el cálculo nuevamente por favor!","WARNING MESSAGE", JOptionPane.WARNING_MESSAGE);
			}
		}
	}
	
	/**
	 * initAdminListProcess executes the admin initiaizer process to show.
	 */
	public void initAdminListProcess() {
		Administration[] administration;
		administration = AdministrationClient.getAllAdmins();
		tableModelAdmin.setRowCount(0);
		for (Administration admin : administration) {
			Object[] adminItems = {admin.getIdEmployee(),admin.getEmployeeType(),admin.getEmployeeName(),admin.getEmployeeLastName(),admin.getPhone()};
			tableModelAdmin.addRow(adminItems);
		}
		spListAdmin.setViewportView(tblAdministrators);	
	}
	
	/**
	 * removeSelectedSaleProcess executes the removal process for the tempSale table.
	 */
	public void deleteAdminProcess() {
		int row;
		int idAdmin;
		int statusAdm=200;
		row = tblAdministrators.getSelectedRow();
		if (row > -1) {
			idAdmin = (Integer) tblAdministrators.getValueAt(row, 0);
			statusAdm=AdministrationClient.deleteAdmin(idAdmin);
			if(statusAdm==200) {
			tableModelAdmin.setRowCount(0);
			initAdminListProcess();
			JOptionPane.showMessageDialog(null,"Administrador Borrado!","Administración", JOptionPane.INFORMATION_MESSAGE);	
			}else {
				JOptionPane.showMessageDialog(null,"Problemas al tratar de borrar administrador","Administración", JOptionPane.WARNING_MESSAGE);	
			}
		} else {
			JOptionPane.showMessageDialog(null,"Seleccione un administrador a borrar por favor","Administración", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * updateAdminPassProcess executes the updating process for the admin table.
	 */
	@SuppressWarnings("null")
	public void updateAdminPassProcess() {
		int row;
		int idAdmin;
		Administration administration = new Administration();
		String employeeType;
		String name;
		String lastName;
		String phone;
		String newPass = null;
		String tryPass;
		String currentPass;
		int statusAdm=200;
		int passCheck;
		JPasswordField pfcheck = new JPasswordField();
		int passResult;
		JPasswordField pf = new JPasswordField();
		row = tblAdministrators.getSelectedRow();
		if (row > -1) {
			idAdmin = (Integer) tblAdministrators.getValueAt(row, 0);
			employeeType = (String) tblAdministrators.getValueAt(row, 1);
			name = (String) tblAdministrators.getValueAt(row, 2);
			lastName = (String) tblAdministrators.getValueAt(row, 3);
			phone = (String) tblAdministrators.getValueAt(row, 4);
			administration = AdministrationClient.getAdminById(idAdmin);
			currentPass= administration.getPass();
			administration = new Administration();
			passCheck=JOptionPane.showConfirmDialog(null, pfcheck, "Contraseña Actual:", JOptionPane.OK_OPTION, JOptionPane.PLAIN_MESSAGE);
			if (passCheck == JOptionPane.OK_OPTION) {
				tryPass = new String(pfcheck.getPassword());
				if( tryPass.equals(currentPass)) {
					passResult=JOptionPane.showConfirmDialog(null, pf, "Nueva Contraseña:", JOptionPane.OK_OPTION, JOptionPane.PLAIN_MESSAGE);
					if (passResult == JOptionPane.OK_OPTION) {
						newPass = new String(pf.getPassword());
							if(!newPass.isEmpty()) {
							administration.setIdEmployee(idAdmin);
							administration.setEmployeeType(employeeType);
							administration.setEmployeeName(name);
							administration.setEmployeeLastName(lastName);
							administration.setPhone(phone);
							administration.setPass(newPass);
							statusAdm=AdministrationClient.updateAdmin(administration);
							if(statusAdm==200) {
							tableModelAdmin.setRowCount(0);
							initAdminListProcess();
							JOptionPane.showMessageDialog(null,"Actualizado Correctamente!","Administración", JOptionPane.INFORMATION_MESSAGE);	
							}else {
								JOptionPane.showMessageDialog(null,"Problemas al tratar de actualizar administrador","Administración", JOptionPane.WARNING_MESSAGE);	
							}
							}else {
								JOptionPane.showMessageDialog(null,"No ingreso ningun dato","Administración", JOptionPane.WARNING_MESSAGE);	
							}
						}
				}else {
					JOptionPane.showMessageDialog(null,"Su contraseña no coincide con la actual","Administración", JOptionPane.INFORMATION_MESSAGE);	
				}
			}
		} else {
			JOptionPane.showMessageDialog(null,"Seleccione un administrador a actualizar por favor","Administración", JOptionPane.WARNING_MESSAGE);
		}
	}
	
	/**
	 * saveAdminProcess executes the saving process for the Admin table.
	 */
	public void saveAdminProcess() {
		Administration admin = new Administration();
		String employeeName;
		String lastName;
		String employeeType;
		String phone;
		String passCheck;
		String passFinal;
		int statusAdmin=200;
		if(txtAdmNombre.getText().isEmpty() || txtAdmApellido.getText().isEmpty() || txtPassUno.getText().isEmpty()
			|| txtAdmTel.getText().isEmpty() ||	txtPassDos.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null,"Favor de completar los campos","Administración", JOptionPane.WARNING_MESSAGE);		
		}else {
			employeeName = txtAdmNombre.getText();
			lastName = txtAdmApellido.getText();
			employeeType =(String) cmbEmployeeType.getSelectedItem();
			phone = txtAdmTel.getText();
			passCheck = txtPassUno.getText();
			passFinal = txtPassDos.getText();
			if(passCheck.equals(passFinal)) {
				admin.setEmployeeName(employeeName);
				admin.setEmployeeLastName(lastName);
				admin.setEmployeeType(employeeType);
				admin.setPhone(phone);
				admin.setPass(passFinal);
				statusAdmin=AdministrationClient.addAdmin(admin);
				if(statusAdmin==200) {
					JOptionPane.showMessageDialog(null,"Bienvenid@ "+employeeName+" "+lastName,"Administración", JOptionPane.WARNING_MESSAGE);
					txtAdmNombre.setText(null);
					txtAdmApellido.setText(null);
					txtAdmTel.setText(null);
					txtPassUno.setText(null);
					txtPassDos.setText(null);
					initAdminListProcess();
				}else {
					JOptionPane.showMessageDialog(null,"Problemas al intentar guardar Administrador","Administración", JOptionPane.WARNING_MESSAGE);
				}
			}else {
				JOptionPane.showMessageDialog(null,"Contraseñas no coinciden!","Administración", JOptionPane.WARNING_MESSAGE);	
			}
		}
	}
	
	/**
	 * generateOperationProcess executes the operation process for the IN/OUT tables.
	 */
	public void generateOperationProcess() {
		double total;
		String totalQuantity;
		String description;
		String operation;
		String paymentType;
		String operationDate;
		boolean creditCard=false;
		int status=200;
		if(txtOpCantidad.getText().isEmpty() || txtOpDescription.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null,"Completar los campos por favor!","Operaciones", JOptionPane.WARNING_MESSAGE);
		}else {
			totalQuantity = txtOpCantidad.getText();
			description = txtOpDescription.getText();
			operation = (String)cmbOpType.getSelectedItem();
			paymentType = (String)cmbAdminOpPayType.getSelectedItem();
			operationDate = TempSaleCalculating.getCurrentTimeUsingDate();
			if(paymentType.equals(PaymentTypeCatalog.CARD)) {
				creditCard=true;
			}
			if(operation.equals("ENTRADA")) {
				try {
				Incomes income = new Incomes();
				total = Double.parseDouble(totalQuantity);
				income.setNoSale(noVenta);
				income.setDescription(description);
				income.setPaymentType(paymentType);
				income.setTotal(total);
				income.setIncomeDate(operationDate);
				income.setEmployee(employee);
				status = IncomesClient.addIncome(income);
				if(status==200) {
					Cash cash = new Cash();
					cash = TempSaleCalculating.updateCashProcess(total,creditCard);
					noVenta = cash.getNoSale();
					cashQuantity = cash.getQuantity();
					JOptionPane.showMessageDialog(null,operation+" Generada Correctamente!","Operaciones", JOptionPane.WARNING_MESSAGE);
					txtOpCantidad.setText(null);
					txtOpDescription.setText(null);
					generateBalanceProcess(false);
				}
				}catch(NumberFormatException nfe) {
					JOptionPane.showMessageDialog(null,"Ingrese número en el campo 'total' porfavor!","Operaciones", JOptionPane.WARNING_MESSAGE);	
				}
				
			}else {
				if(cashQuantity > Double.parseDouble(totalQuantity)) {
				try {
					Outgoings outs = new Outgoings();
					total = Double.parseDouble(totalQuantity);
					outs.setDescription(description);
					outs.setPaymentType(paymentType);
					outs.setTotal(total);
					outs.setOutgoingDate(operationDate);
					outs.setEmployee(employee);
					status = OutgoingsClient.addOutGoing(outs);
					if(status==200) {
						Cash cash = new Cash();
						cash = TempSaleCalculating.updateCashProcess(total,creditCard,true);
						noVenta = cash.getNoSale();
						cashQuantity = cash.getQuantity();
						JOptionPane.showMessageDialog(null,operation+" Generada Correctamente!","Operaciones", JOptionPane.WARNING_MESSAGE);
						txtOpCantidad.setText(null);
						txtOpDescription.setText(null);
						generateBalanceProcess(false);
					}
					}catch(NumberFormatException nfe) {
						JOptionPane.showMessageDialog(null,"Ingrese número en el campo 'total' porfavor!","Operaciones", JOptionPane.WARNING_MESSAGE);	
					}
				}else {
					JOptionPane.showMessageDialog(null,"La cantidad a retirar es mayor a la existente, verifique por favor!","Operaciones", JOptionPane.WARNING_MESSAGE);
				}
			}
		}

	}
	
	/**
	 * printSaleProcesss executes the print process for the sale table.
	 * @throws PrinterException 
	 * @throws InterruptedException 
	 */
	public void printSaleProcess(SaleTicketModel saleTicketModel,String client) throws PrinterException, InterruptedException {
            	SaleTicket.salePrintClientTicket(saleTicketModel,client);
                Thread.sleep(5000);
                SaleTicket.salePrintServiceTicket(saleTicketModel,client); 
	}
	
	
	}
