package com.project.newyorkcofee.mails;

import com.project.newyorkcoffee.model.Closure;

public class MailAuthData {
	
	public final static String USERNAME="J3FPukD52orOWTfXdvMDlUUkkdXz2cs3QW7EK7rmy7M=";
    public final static String PASSWORD="8em8suYn41RINkLlE8N6bw==";
    public final static String DESTINATION="J3FPukD52orOWTfXdvMDlUUkkdXz2cs3QW7EK7rmy7M=";
    public final static String SMPT_AUTH="mail.smtp.auth";
    public final static String SMPT_STARTTLS="mail.smtp.starttls.enable";
    public final static String SMPT_HOST="mail.smtp.host";
    public final static String GMAIL_HOST="smtp.gmail.com";
    public final static String SMTP_PORT="mail.smtp.port";
    public final static String SMTP_ENABLE="true";
    public final static String SMTP_PORT_NUMBER="587";

    public static String bodyMailClosureBuilder(Closure closureData) {
    	String initCash,totalCard,totalCash,totalIn,totalOut,totalGenerated,employee,closureDate;
        String bodyClosureMail;
    	initCash = closureData.getInitCash().toString();
        totalCard = closureData.getTotalCard().toString();
        totalCash = closureData.getTotalCash().toString();
        totalIn = closureData.getTotalIn().toString();
        totalOut = closureData.getTotalOut().toString();
        totalGenerated = closureData.getTotalGenerated().toString();
        closureDate = closureData.getClosureDate();
        employee = closureData.getEmployeeName();
 
	        bodyClosureMail = "*** NEWYORK COFFEE ***\n"+
	    "\t CORTE DE CAJA\n"+    
	    "=============================\n"+
	    "DETALLE DE CORTE\n"+           
	    "=============================\n"+
	    "INICIO CAJA: "+initCash+
	    "\nTOTAL ENTRADAS: "+totalIn+
	    "\nTOTAL SALIDAS: "+totalOut+
	    "\nTOTAL EFECTIVO: "+totalCash+
	    "\nTOTAL TARJETA: "+totalCard+
	    "\n\n   TOTAL GENERADO: "+totalGenerated+              
	    "\n"+
	    "\n=============================\n"+
	    "ATENDIO:\t"+employee+
	    "\nFECHA: "+closureDate+
	    "\n=============================\n"+
	    "\n"+
	    "\tEN SISTEMAS ESTAMOS PARA SERVIRLE\n"+
	    "\tGRACIAS POR SU PREFERENCIA\n"+
	    "\tSALUDOS CORDIALES DE PARTE DE ING.PEREA\n"+
	    "\n\n\n\n\n\n"; 
	  
	   return bodyClosureMail;
    	
    }

}
