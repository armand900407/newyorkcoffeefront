package com.project.newyorkcofee.mails;

import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;

import com.project.newyorkcoffee.model.Closure;

public class SendClosureMail {
	    
	public static boolean SendMail(Closure closureData) throws Exception { 
	   
        String bodyClosureMail;
        boolean sentOk=true;
	        bodyClosureMail = MailAuthData.bodyMailClosureBuilder(closureData);
	        
	        Properties props = new Properties();
	        props.put(MailAuthData.SMPT_AUTH, MailAuthData.SMTP_ENABLE);
	        props.put(MailAuthData.SMPT_STARTTLS,  MailAuthData.SMTP_ENABLE);
	        props.put(MailAuthData.SMPT_HOST, MailAuthData.GMAIL_HOST);
	        props.put(MailAuthData.SMTP_PORT, MailAuthData.SMTP_PORT_NUMBER);
	 
	        Session session = Session.getInstance(props,
	                new javax.mail.Authenticator() {
	                    protected PasswordAuthentication getPasswordAuthentication() {
	                        try {
								return new PasswordAuthentication(EncryptSecurity.decrypt(MailAuthData.USERNAME), EncryptSecurity.decrypt(MailAuthData.PASSWORD));
							} catch (Exception e) {
								System.out.println("Problemas al descifrar usuario y constraseña");
								e.printStackTrace();
							}
							return null;
	                    }
	                });
	 
	        try {
	            
	         // Create the message part
	         BodyPart messageBodyPart = new MimeBodyPart();
	         
	         // Now set the actual message
	         messageBodyPart.setText(bodyClosureMail);
	         // Create a multipar message
	         Multipart multipart = new MimeMultipart();
	         // Set text message part
	         multipart.addBodyPart(messageBodyPart);

	            Message message = new MimeMessage(session);
	            message.setFrom(new InternetAddress(EncryptSecurity.decrypt(MailAuthData.USERNAME)));
	            message.setRecipients(Message.RecipientType.TO,
	                    InternetAddress.parse(EncryptSecurity.decrypt(MailAuthData.DESTINATION)));
//	            message.setRecipients(Message.RecipientType.CC,
//	                    InternetAddress.parse("terry900407@gmail.com"));
	            message.setSubject("Notificación de corte de "+closureData.getEmployeeName());
	            message.setText(bodyClosureMail);            
	            // Part two is attachment 
	            message.setContent(multipart);
	            Transport.send(message);
	 
	        } catch (MessagingException e) {
	            JOptionPane.showMessageDialog(null,e);
	            JOptionPane.showMessageDialog(null,"Favor de enviar Reportes a : makeuplifeadm@gmail.com ");
	            sentOk=false;
	            return sentOk;      
	        }
	   return sentOk;
	}
	     
//	     public static void main(String[] args) {
//	    	 Closure closure = new Closure();
//	    	 closure.setIdClosure(1);
//	    	 closure.setEmployeeName("Putina Sanchez");
//	    	 closure.setInitCash(500.0);
//	    	 closure.setTotalIn(2500.0);
//	    	 closure.setTotalOut(200.0);
//	    	 closure.setTotalCash(2300.0);
//	    	 closure.setTotalCard(150.0);
//	    	 closure.setTotalGenerated(2000.0);
//	    	 closure.setClosureDate("8 Dic 2018");
//	           SendClosureMail.SendMail(closure); 
//	     	}
	     

}
