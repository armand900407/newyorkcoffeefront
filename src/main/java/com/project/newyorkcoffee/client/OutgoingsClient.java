package com.project.newyorkcoffee.client;

import java.net.URI;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.project.newyorkcoffee.model.Incomes;
import com.project.newyorkcoffee.model.Outgoings;
import com.project.newyorkcoffee.model.TemporarySale;

public class OutgoingsClient {
	
	public static Outgoings getOutGoingById(Integer idOutgoing) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/outgoings/getOutgoing/{id}";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Outgoings> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Outgoings.class, idOutgoing);
        Outgoings outgoings = responseEntity.getBody();
        System.out.println("INFO outgoings: "+outgoings.toString());  
        return outgoings;
    }
	
    public static Outgoings[] getAllOutGoings() {
	HttpHeaders headers = new HttpHeaders();
	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/outgoings/getAll";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Outgoings[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Outgoings[].class);
        Outgoings[] outgoings = responseEntity.getBody();
        return outgoings;
    }
    
    public static Double getAllOutGoingsCash() {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	double totalCash=0.0;
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080/newyorkcoffee/v1/outgoings/getAllCash";
            HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
            try {
            ResponseEntity<Double> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Double.class);
            totalCash = responseEntity.getBody();
    		}catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    				if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    						status = httpClientOrServerExc.getRawStatusCode();
    						System.out.println("Status from outgoings/getAllCash : "+status);
    						totalCash=0.0;
    				}
    				return totalCash;
    		}
    		return totalCash;
        }
    
    public static Double getAllOutGoingsCard() {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	double totalCard=0.0;
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080/newyorkcoffee/v1/outgoings/getAllCard";
            HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
            try {
            ResponseEntity<Double> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Double.class);
            totalCard = responseEntity.getBody();
    		}catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    			if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    				status = httpClientOrServerExc.getRawStatusCode();
    				System.out.println("Status from outgoings/getAllCard : "+status);
    				totalCard=0.0;
    			}
    			return totalCard;
    		}
            return totalCard;
        }
    
    public static Integer addOutGoing(final Outgoings outgoing) {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/outgoings/createOutgoing";
        HttpEntity<Outgoings> requestEntity = new HttpEntity<Outgoings>(outgoing, headers);
        try {
        URI uri = restTemplate.postForLocation(url, requestEntity);
        }catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
        	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
        	      status = httpClientOrServerExc.getRawStatusCode();
        	    }
        	return status;
        }
        return status; 
    }
    
    public static void updateOutGoing(final Outgoings outgoings) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/outgoings/updateOutgoing";
        HttpEntity<Outgoings> requestEntity = new HttpEntity<Outgoings>(outgoings, headers);
        restTemplate.put(url, requestEntity);
        System.out.println("restTemplate Updated: "+restTemplate); 
    }
    
    public static void deleteOutGoing(Integer idOutgoing) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/outgoings/deleteOutgoing/{id}";
        HttpEntity<Outgoings> requestEntity = new HttpEntity<Outgoings>(headers);
        restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, Void.class, idOutgoing); 
        System.out.println("restTemplate Deleted: "+restTemplate); 
    }
    
    public static Integer truncateOutgoings() {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/outgoings/truncateOutgoings";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        HttpEntity<TemporarySale> responseEntity = new HttpEntity<>(headers);
        try {
        restTemplate.exchange(url, HttpMethod.GET, requestEntity, Void.class);
    	}catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    	      status = httpClientOrServerExc.getRawStatusCode();
    	    }
    	return status;
    }
    return status; 
    }
    
}
