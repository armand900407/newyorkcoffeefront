package com.project.newyorkcoffee.client;

import java.net.URI;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.project.newyorkcoffee.model.Products;

public class ProductClient {
	
	public static Products getProductById(Integer idProduct) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/product/getProd/{id}";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Products> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Products.class, idProduct);
        Products product = responseEntity.getBody();
        System.out.println("INFO PRODUCT: "+product.toString());  
        return product;
    }
	
	public static Products[] getProductByName(String prodName) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/product/getProdByName/{prodName}";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Products[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Products[].class, prodName);
        Products[] product = responseEntity.getBody();
        System.out.println("INFO PRODUCT: "+product.toString());  
        return product;
    }
	
    public static Products[] getAllProducts() {
	HttpHeaders headers = new HttpHeaders();
	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/product/getAll";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Products[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Products[].class);
        Products[] products = responseEntity.getBody();
        for(Products prod : products) {
        	System.out.println("INFO: "+prod.toString());
        }
        return products;
    }
    
    public static Products[] getHotDrinks() {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
            RestTemplate restTemplate = new RestTemplate();
    	String url = "http://localhost:8080/newyorkcoffee/v1/product/getHotDrinks";
            HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
            ResponseEntity<Products[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Products[].class);
            Products[] products = responseEntity.getBody();
            for(Products prod : products) {
            	System.out.println("INFO: "+prod.toString());
            }
            return products;
    }
    
    public static Products[] getColdDrinks() {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080/newyorkcoffee/v1/product/getColdDrinks";
            HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
            ResponseEntity<Products[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Products[].class);
            Products[] products = responseEntity.getBody();
            for(Products prod : products) {
            	System.out.println("INFO: "+prod.toString());
            }
            return products;
    }
    
    public static Products[] getCocktailDrinks() {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080/newyorkcoffee/v1/product/getCocktailDrinks";
            HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
            ResponseEntity<Products[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Products[].class);
            Products[] products = responseEntity.getBody();
            for(Products prod : products) {
            	System.out.println("INFO: "+prod.toString());
            }
            return products;
    }
    
    public static Products[] getFood() {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080/newyorkcoffee/v1/product/getFood";
            HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
            ResponseEntity<Products[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Products[].class);
            Products[] products = responseEntity.getBody();
            for(Products prod : products) {
            	System.out.println("INFO: "+prod.toString());
            }
            return products;
    }
    
    public static Products[] getDrinkComplement() {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080/newyorkcoffee/v1/product/getDrinkComplement";
            HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
            ResponseEntity<Products[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Products[].class);
            Products[] products = responseEntity.getBody();
            for(Products prod : products) {
            	System.out.println("INFO: "+prod.toString());
            }
            return products;
    }
    
    public static Products[] getFoodComplement() {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080/newyorkcoffee/v1/product/getFoodComplement";
            HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
            ResponseEntity<Products[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Products[].class);
            Products[] products = responseEntity.getBody();
            for(Products prod : products) {
            	System.out.println("INFO: "+prod.toString());
            }
            return products;
    }
    
    public static Integer addProduct(final Products product) {
    	int status=200;
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/product/createProd";
        HttpEntity<Products> requestEntity = new HttpEntity<Products>(product, headers);
        try {
        URI uri = restTemplate.postForLocation(url, requestEntity);
        }catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    	      status = httpClientOrServerExc.getRawStatusCode();
    	    }
    	return status;
    	}
        return status;    	
    }
    
    public static Integer updateProduct(final Products product) {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/product/updateProd";
        HttpEntity<Products> requestEntity = new HttpEntity<Products>(product, headers);
        try {
        restTemplate.put(url, requestEntity);
        }catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
        	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
        	      status = httpClientOrServerExc.getRawStatusCode();
        	    }
        	return status;
        }
        return status; 
    }
    
    public static Integer deleteProduct(Integer idProd) {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/product/deleteProd/{id}";
        HttpEntity<Products> requestEntity = new HttpEntity<Products>(headers);
        try {
        restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, Void.class, idProd); 
        }catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
        	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
        	      status = httpClientOrServerExc.getRawStatusCode();
        	    }
        	return status;
        	}
            return status; 
    }
}
