package com.project.newyorkcoffee.client;

import java.net.URI;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.project.newyorkcoffee.model.Closure;

public class ClosureClient {

	public static Closure getClosureById(Integer idClosure) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/closure/getClosure/{id}";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Closure> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Closure.class, idClosure);
        Closure closure = responseEntity.getBody();
        System.out.println("INFO Closure: "+closure.toString());  
        return closure;
    }
	
    public static Closure[] getAllClosures() {
	HttpHeaders headers = new HttpHeaders();
	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/closure/getAll";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Closure[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Closure[].class);
        Closure[] closure = responseEntity.getBody();
        return closure;
    }
    
    public static Integer addClosure(final Closure closure) {
    	HttpHeaders headers = new HttpHeaders();
    	int status = 200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/closure/createClosure";
        HttpEntity<Closure> requestEntity = new HttpEntity<Closure>(closure, headers);
        try {
        URI uri = restTemplate.postForLocation(url, requestEntity);
    	}catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    	      status = httpClientOrServerExc.getRawStatusCode();
    	    }
    	return status;
    }
    return status; 
    }
    
    public static void updateClosure(final Closure closure) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/closure/updateClosure";
        HttpEntity<Closure> requestEntity = new HttpEntity<Closure>(closure, headers);
        restTemplate.put(url, requestEntity);
        System.out.println("restTemplate Updated: "+restTemplate); 
    }
    
    public static void deleteClosure(Integer idClosure) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/closure/deleteClosure/{id}";
        HttpEntity<Closure> requestEntity = new HttpEntity<Closure>(headers);
        restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, Void.class, idClosure); 
        System.out.println("restTemplate Deleted: "+restTemplate); 
    }
    
}
