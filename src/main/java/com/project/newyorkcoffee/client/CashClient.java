package com.project.newyorkcoffee.client;

import java.net.URI;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.project.newyorkcoffee.model.Cash;

public class CashClient {
	
	public static Cash getCashById(Integer idCash) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/cash/getCash/{id}";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Cash> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Cash.class, idCash);
        Cash cash = responseEntity.getBody(); 
        return cash;
    }
	
    public static Cash[] getAllCash() {
	HttpHeaders headers = new HttpHeaders();
	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/cash/getAll";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Cash[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Cash[].class);
        Cash[] cash = responseEntity.getBody();
        return cash;
    }
    
    public static void addCash(final Cash cash) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/cash/createCash";
        HttpEntity<Cash> requestEntity = new HttpEntity<Cash>(cash, headers);
        URI uri = restTemplate.postForLocation(url, requestEntity);   	
    }
    
    public static boolean updateCash(final Cash cash) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/cash/updateCash";
        boolean updateOk=true;
        int status;
        try {
        HttpEntity<Cash> requestEntity = new HttpEntity<Cash>(cash, headers);
        restTemplate.put(url, requestEntity);
    	}catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    	      status = httpClientOrServerExc.getRawStatusCode();
    	      updateOk=false;
    	    }
    	return updateOk;
    	}
    	return updateOk;
    }
    
    public static void deleteCash(Integer idCash) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/cash/deleteCash/{id}";
        HttpEntity<Cash> requestEntity = new HttpEntity<Cash>(headers);
        restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, Void.class, idCash); 
        System.out.println("restTemplate Deleted: "+restTemplate); 
    }
}
