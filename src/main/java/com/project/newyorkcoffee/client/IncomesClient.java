package com.project.newyorkcoffee.client;

import java.net.URI;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.project.newyorkcoffee.model.Incomes;
import com.project.newyorkcoffee.model.TemporarySale;

public class IncomesClient {

	public static Incomes getIncomeById(Integer idIncome) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/incomes/getIncome/{id}";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Incomes> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Incomes.class, idIncome);
        Incomes incomes = responseEntity.getBody(); 
        return incomes;
    }
	
    public static Incomes[] getAllIncomes() {
	HttpHeaders headers = new HttpHeaders();
	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/incomes/getAll";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        ResponseEntity<Incomes[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Incomes[].class);
        Incomes[] income = responseEntity.getBody();
        return income;
    }
    
    public static Double getAllIncomesCash() {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	double totalCash=0.0;
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080/newyorkcoffee/v1/incomes/getAllCash";
            HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
            try {
            ResponseEntity<Double> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Double.class);
            totalCash = responseEntity.getBody();
    		}catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    			if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    				status = httpClientOrServerExc.getRawStatusCode();
    				System.out.println("Status from incomes/getAllCash : "+status);
    				totalCash=0.0;
    			}
    			return totalCash;
    		}
		return totalCash;
    }
    
    public static Double getAllIncomesCards() {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	double totalCard=0.0;
    	int status=200;
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080/newyorkcoffee/v1/incomes/getAllCard";
            HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
            try {
            ResponseEntity<Double> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Double.class);
            totalCard = responseEntity.getBody();
    		}catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    				if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    					status = httpClientOrServerExc.getRawStatusCode();
    					System.out.println("Status from incomes/getAllCard : "+status);
    					totalCard=0.0;
    				}
    				return totalCard;
    		}
    		return totalCard;
        }
    
    public static Integer addIncome(final Incomes income) {
    	HttpHeaders headers = new HttpHeaders();
    	int status = 200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/incomes/createIncome";
        HttpEntity<Incomes> requestEntity = new HttpEntity<Incomes>(income, headers);
        try {
        URI uri = restTemplate.postForLocation(url, requestEntity);
    	}catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    	      status = httpClientOrServerExc.getRawStatusCode();
    	    }
    	return status;
    	}
        return status; 	
    }
    
    public static Integer updateIncome(final Incomes income) {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/incomes/updateIncome";
        HttpEntity<Incomes> requestEntity = new HttpEntity<Incomes>(income, headers);
        try {
        restTemplate.put(url, requestEntity);
    	}catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    	      status = httpClientOrServerExc.getRawStatusCode();
    	    }
    	return status;
    	}
        return status; 	
    }
    
    public static void deleteIncome(Integer idIncome) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/incomes/deleteIncome/{id}";
        HttpEntity<Incomes> requestEntity = new HttpEntity<Incomes>(headers);
        restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, Void.class, idIncome); 
        System.out.println("restTemplate Deleted: "+restTemplate); 
    }
    
    public static Integer truncateIncomes() {
    	HttpHeaders headers = new HttpHeaders();
    	int status=200;
    	headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/newyorkcoffee/v1/incomes/truncateIncomes";
        HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
        HttpEntity<TemporarySale> responseEntity = new HttpEntity<>(headers);
        try {
        restTemplate.exchange(url, HttpMethod.GET, requestEntity, Void.class);
    	}catch(HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
    	if(HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode()) || HttpStatus.INTERNAL_SERVER_ERROR.equals(httpClientOrServerExc.getStatusCode())) {
    	      status = httpClientOrServerExc.getRawStatusCode();
    	    }
    	return status;
    }
    return status; 
    }
    
}
