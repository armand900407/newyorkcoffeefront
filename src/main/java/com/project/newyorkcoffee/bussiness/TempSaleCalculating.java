package com.project.newyorkcoffee.bussiness;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.project.newyorkcoffee.catalogs.PaymentTypeCatalog;
import com.project.newyorkcoffee.client.CashClient;
import com.project.newyorkcoffee.model.Cash;
import com.project.newyorkcoffee.model.TemporarySale;

public class TempSaleCalculating {
	
	public static String getTotalTempSale(TemporarySale[] tempSale) {
		double cantidad=0.0;
		String finalTotal;
		for (TemporarySale temporarySale : tempSale) {
			cantidad = cantidad+temporarySale.getTotal();
		}
		finalTotal = String.valueOf(cantidad);
		return finalTotal;
	}
	
	public static Double getProductComboPrice(String comboInfo) {
		String[] infoList;
		double precio;
		infoList = comboInfo.split("\\$");
		precio = Double.parseDouble(infoList[1]);
		return precio;
	}
	
	public static String getProductComboDescription(String comboInfo) {
		String[] infoList;
		String description;
		infoList = comboInfo.split("\\$");
		description = infoList[0];
		description = description.trim();
		return description;
	}
	
	public static Double getTotalFromQuantity(int quantity,double total) {
		double totalFinal;
		totalFinal = total*quantity;
		return totalFinal;
	}
	
	public static Double getTotalWithComplement(int quantity,double complement, double total) {
		double totalFinal;
		totalFinal = total+(complement*quantity);
		return totalFinal;
	}
	
	public static String getChangeSale(String sTotal,String sReceived) {
		double total;
		double received;
		double change;
		String finalChange="0.0";
		try {
		total = Double.parseDouble(sTotal);
		received = Double.parseDouble(sReceived);
		change = received-total;
		finalChange = String.valueOf(change);
		}catch(NumberFormatException nf) {
			System.out.println("Number Format not valid, no letters allowed at change field");
		}
		return finalChange;
	}
	
	public static String getCurrentTimeUsingDate() {
        Date date = new Date();
        Long datelong = date.getTime();
        String strDateFormat = "dd MMMM yyyy hh:mm:ss a";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
        String formattedDate= dateFormat.format(date);
        return formattedDate;
    }
	
	public static Cash updateCashProcess(double total) {
		Cash cash = new Cash();
		double currentCash;
		double newCash;
		int idCash=PaymentTypeCatalog.ID_CASH;
		int noSale;
		cash = CashClient.getCashById(idCash);
		currentCash = cash.getQuantity();
		noSale = cash.getNoSale();
		newCash = currentCash+total;
		noSale++;
		cash.setNoSale(noSale);
		cash.setQuantity(newCash);
		CashClient.updateCash(cash);
        return cash;
    }
	
	public static Cash updateCashProcess(double total,boolean creditCard) {
		Cash cash = new Cash();
		double currentCash;
		double newCash;
		int idCash=PaymentTypeCatalog.ID_CASH;
		int noSale;
		cash = CashClient.getCashById(idCash);
		currentCash = cash.getQuantity();
		noSale = cash.getNoSale();
		newCash = currentCash+total;
		noSale++;
		cash.setNoSale(noSale);
		if(creditCard) {
		cash.setQuantity(currentCash);
		}else {
		cash.setQuantity(newCash);	
		}
		CashClient.updateCash(cash);
        return cash;
    }
	
	public static Cash updateCashProcess(double total,boolean creditCard,boolean cancellation) {
		Cash cash = new Cash();
		double currentCash;
		double newCash;
		int idCash=PaymentTypeCatalog.ID_CASH;
		int noSale;
		cash = CashClient.getCashById(idCash);
		currentCash = cash.getQuantity();
		noSale = cash.getNoSale();
		if(cancellation && creditCard) {
		cash.setQuantity(currentCash);	
		cash.setNoSale(noSale);
		}
		else if(cancellation && !creditCard){
		newCash = currentCash-total;
		cash.setQuantity(newCash);	
		cash.setNoSale(noSale);	
		}
		CashClient.updateCash(cash);
        return cash;
    }
	
	public static Integer getNoSale() {
		Cash cash = new Cash();
		int noSale;
		cash = CashClient.getCashById(PaymentTypeCatalog.ID_CASH);
		noSale = cash.getNoSale();
        return noSale;
    }
	
	public static Double getCashQuantity() {
		Cash cash = new Cash();
		double cashQuantity;
		cash = CashClient.getCashById(PaymentTypeCatalog.ID_CASH);
		cashQuantity = cash.getQuantity();
        return cashQuantity;
    }
	
    
//    public static void main(String[] args) {
//    	//TempSaleCalculating.getCurrentTimeUsingDate();
//	}

}
