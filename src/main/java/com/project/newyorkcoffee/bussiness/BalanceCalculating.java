package com.project.newyorkcoffee.bussiness;

import com.project.newyorkcoffee.model.Incomes;
import com.project.newyorkcoffee.model.Outgoings;
import com.project.newyorkcoffee.model.Sales;

public class BalanceCalculating {
	
	public static String getTotalIncomes(Incomes[] incomes) {
		double cantidad=0.0;
		String finalTotal;
		for (Incomes ins : incomes) {
			cantidad = cantidad+ins.getTotal();
		}
		finalTotal = String.valueOf(cantidad);
		return finalTotal;
	}
	
	public static String getTotalOutGoings(Outgoings[] outgoings) {
		double cantidad=0.0;
		String finalTotal;
		for (Outgoings outs : outgoings) {
			cantidad = cantidad+outs.getTotal();
		}
		finalTotal = String.valueOf(cantidad);
		return finalTotal;
	}
	
	public static double getBalance(String ins, String outs, double initcash) {
		double incomes;
		double outgoings;
		double result=0;
		
		try {
			incomes = Double.parseDouble(ins);
			outgoings = Double.parseDouble(outs);
			result = (incomes-outgoings)-initcash;
		}catch(NumberFormatException nfe) {
			System.out.println("Not valid conversion for incomes or outgoings");
		}
		return result;
	}
	
	public static String getTotalSaleList(Sales[] sales) {
		double cantidad=0.0;
		String finalTotal;
		for (Sales sa : sales) {
			cantidad = cantidad+sa.getTotal();
		}
		finalTotal = String.valueOf(cantidad);
		return finalTotal;
	}

}
