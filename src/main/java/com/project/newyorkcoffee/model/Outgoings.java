package com.project.newyorkcoffee.model;

import java.io.Serializable;
import java.util.Objects;

public class Outgoings implements Serializable {
	/**
	 * Serial Version.
	 */
	private static final long serialVersionUID = -7705889824084179823L;
	
	/**
	 * idOutgoing that corresponds to the idOutgoing.
	 */
	private Integer idOutgoing;
	/**
	 * description that corresponds to the product description bought.
	 */
	private String description;
	/**
	 * paymentType that corresponds to the paymentType.
	 */
	private String paymentType;
	/**
	 * total that corresponds to the paymentType.
	 */
	private Double total;
	/**
	 * outgoingDate that corresponds to the paymentType.
	 */
	private String outgoingDate;
	/**
	 * employee that corresponds to the employee.
	 */
	private String employee;
	
	/**
	 * Constructor with Arguments for Outgoings.
	 */
	public Outgoings(Integer idOutgoing, String description, String paymentType, Double total, String outgoingDate, String employee) {
		this.idOutgoing = idOutgoing;
		this.description = description;
		this.paymentType = paymentType;
		this.total = total;
		this.outgoingDate = outgoingDate;
		this.employee = employee;
	}
	
	/**
	 * Constructor with No Arguments for Outgoings.
	 */
	public Outgoings() {
	}

	/**
	 * hashCode.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(idOutgoing, description, paymentType, total, outgoingDate, employee);
	}

	/**
	 * equals.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Outgoings other = (Outgoings) obj;
		return Objects.equals(idOutgoing, other.idOutgoing) && Objects.equals(description, other.description) 
				&& Objects.equals(paymentType, other.paymentType)
				&& Objects.equals(total, other.total)
				&& Objects.equals(outgoingDate, other.outgoingDate)
				&& Objects.equals(employee, other.employee);
	}

	/**
	 * toString.
	 */
	@Override
	public String toString() {
		return "Outgoings [idOutgoing=" + idOutgoing + ", description=" + description + ", paymentType=" + paymentType+ 
				", total=" + total	+ ", outgoingDate=" + outgoingDate + ", employee=" + employee + "]";
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Integer getIdOutgoing() {
		return idOutgoing;
	}

	public void setIdOutgoing(Integer idOutgoing) {
		this.idOutgoing = idOutgoing;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getOutgoingDate() {
		return outgoingDate;
	}

	public void setOutgoingDate(String outgoingDate) {
		this.outgoingDate = outgoingDate;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

}
