package com.project.newyorkcoffee.model;

import java.io.Serializable;
import java.util.Objects;

public class Sales implements Serializable{

	/**
	 * Serial Version.
	 */
	private static final long serialVersionUID = 4756356424771277977L;
	/**
	 * idSale that corresponds to the idSale.
	 */
	private Integer idSale;
	/**
	 * noSale that corresponds to the noSale.
	 */
	private Integer noSale;
	/**
	 * description that corresponds to the description.
	 */
	private String description;
	/**
	 * paymentType that corresponds to the paymentType.
	 */
	private String paymentType;
	/**
	 * total that corresponds to the total.
	 */
	private Double total;
	/**
	 * price that corresponds to the price.
	 */
	private String saleDate;
	/**
	 * Constructor with Arguments for Sales.
	 */
	public Sales(Integer idSale, Integer noSale, String description, String paymentType,Double total, String saleDate) {
		this.idSale = idSale;
		this.noSale = noSale;
		this.description = description;
		this.paymentType = paymentType;
		this.total = total;
		this.saleDate = saleDate;
	}
	/**
	 * Constructor with No Arguments.
	 */
	public Sales() {
	}
	
	/**
	 * hashCode.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(idSale, noSale, description, paymentType, total, saleDate);
	}
	
	/**
	 * equals.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sales other = (Sales) obj;
		return Objects.equals(idSale, other.idSale) && Objects.equals(noSale, other.noSale) 
				&& Objects.equals(description, other.description) && Objects.equals(paymentType, other.paymentType)
				&& Objects.equals(total, other.total)
				&& Objects.equals(saleDate, other.saleDate);
	}
	
	/**
	 * toString.
	 */
	@Override
	public String toString() {
		return "Sales [idSale=" + idSale + ", noSale=" + noSale + ", description=" + description + ", paymentType="
				+ paymentType+ ", total="+ total + ", saleDate=" + saleDate + "]";
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Integer getIdSale() {
		return idSale;
	}
	public void setIdSale(Integer idSale) {
		this.idSale = idSale;
	}
	public Integer getNoSale() {
		return noSale;
	}
	public void setNoSale(int i) {
		this.noSale = i;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getSaleDate() {
		return saleDate;
	}
	public void setSaleDate(String saleDate) {
		this.saleDate = saleDate;
	}
	
}
