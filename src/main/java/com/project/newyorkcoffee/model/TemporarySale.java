package com.project.newyorkcoffee.model;

import java.io.Serializable;
import java.util.Objects;

public class TemporarySale implements Serializable{

	/**
	 * Serial Version.
	 */
	private static final long serialVersionUID = -1325557134672054678L;
	/**
	 * idTemporarySale that corresponds to the idTemporarySale.
	 */
	private Integer idTempSale;
	/**
	 * description that corresponds to the description.
	 */
	private String description;
	/**
	 * quantity that corresponds to the product quantity bought.
	 */
	private Integer quantity;
	/**
	 * total that corresponds to the total.
	 */
	private Double total;
	
	/**
	 * Constructor with Arguments for TemporarySale.
	 */
	public TemporarySale(Integer idTempSale, String description, Integer quantity, Double total) {
		this.idTempSale = idTempSale;
		this.description = description;
		this.quantity = quantity;
		this.total = total;
	}

	/**
	 * Constructor with No Arguments for TemporarySale.
	 */
	public TemporarySale() {
	}

	/**
	 * hashCode.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(idTempSale, description, quantity, total);
	}

	/**
	 * equals.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TemporarySale other = (TemporarySale) obj;
		return Objects.equals(idTempSale, other.idTempSale) && Objects.equals(description, other.description) 
				&& Objects.equals(quantity, other.quantity) && Objects.equals(total, other.total);
	}

	/**
	 * toString.
	 */
	@Override
	public String toString() {
		return "TemporarySale [idTempSale=" + idTempSale + ", description=" + description + ", quantity=" + quantity
				+ ", total=" + total + "]";
	}

	public Integer getIdTempSale() {
		return idTempSale;
	}

	public void setIdTempSale(Integer idTempSale) {
		this.idTempSale = idTempSale;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}
	
}
