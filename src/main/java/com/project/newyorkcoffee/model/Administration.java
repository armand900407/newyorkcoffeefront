package com.project.newyorkcoffee.model;

import java.io.Serializable;
import java.util.Objects;

public class Administration implements Serializable{

	/**
	 * Serial Version.
	 */
	private static final long serialVersionUID = -7539847821826752909L;
	/**
	 * idEmployee that corresponds to the employee.
	 */
	private Integer idEmployee;
	/**
	 * employeeType that corresponds to the employeeType.
	 */
	private String employeeType;
	/**
	 * employeeName that corresponds to the employeeName.
	 */
	private String employeeName;
	/**
	 * employeeLastName that corresponds to the employeeLastName.
	 */
	private String employeeLastName;
	/**
	 * phone that corresponds to the phone number.
	 */
	private String phone;
	/**
	 * pass that corresponds to the password.
	 */
	private String pass;
	/**
	 * Constructor with Arguments for Administration.
	 */
	public Administration(Integer idEmployee, String employeeType, String employeeName, String employeeLastName, String phone, String pass) {
		this.idEmployee = idEmployee;
		this.employeeType = employeeType;
		this.employeeName = employeeName;
		this.employeeLastName = employeeLastName;
		this.phone = phone;
		this.pass = pass;
	}
	/**
	 * Constructor with No Arguments.
	 */
	public Administration() {
	}
	
	/**
	 * hashCode.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(idEmployee, employeeType, employeeName, employeeLastName, phone, pass);
	}
	
	/**
	 * equals.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Administration other = (Administration) obj;
		return Objects.equals(idEmployee, other.idEmployee) && Objects.equals(employeeType, other.employeeType) 
				&& Objects.equals(employeeName, other.employeeName) && Objects.equals(employeeLastName, other.employeeLastName)
				&& Objects.equals(phone, other.pass);
	}
	
	/**
	 * toString.
	 */
	@Override
	public String toString() {
		return "Administration [idEmployee=" + idEmployee + ", employeeType=" + employeeType + ", employeeName="
				+ employeeName + ", employeeLastName=" + employeeLastName +", phone=" + phone + ", pass=" + pass + "]";
	}
	public Integer getIdEmployee() {
		return idEmployee;
	}
	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}
	public String getEmployeeType() {
		return employeeType;
	}
	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmployeeLastName() {
		return employeeLastName;
	}
	public void setEmployeeLastName(String employeeLastName) {
		this.employeeLastName = employeeLastName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	
}
