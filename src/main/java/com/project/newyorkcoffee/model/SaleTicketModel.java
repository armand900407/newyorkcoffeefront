package com.project.newyorkcoffee.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

public class SaleTicketModel implements Serializable {

	/**
	 * Serial Version.
	 */
	private static final long serialVersionUID = 7635561542187006721L;

	/**
	 * tempSaleList that corresponds to the idTicket.
	 */
	private TemporarySale[] tempSaleList;
	/**
	 * noVenta that corresponds to the product noVenta bought.
	 */
	private String noSale;
	/**
	 * product that corresponds to the product.
	 */
	private String product;
	/**
	 * payType that corresponds to the product.
	 */
	private String payType;
	/**
	 * total that corresponds to the total.
	 */
	private String total;
	/**
	 * quantity that corresponds to the product quantity bought.
	 */
	private String received;
	/**
	 * change that corresponds to the product quantity bought.
	 */
	private String change;
	/**
	 * saleDate that corresponds to the product quantity bought.
	 */
	private String saleDate;
	/**
	 * employee that corresponds to the product quantity bought.
	 */
	private String employee;
	/**
	 * Constructor with No Arguments for Ticket.
	 */
	public SaleTicketModel() {
	}

	
	/**
	 * Constructor with Arguments for Ticket.
	 */
	public SaleTicketModel(TemporarySale[] tempSaleList, String noSale, String product, String payType, String total,
			String received, String change, String saleDate, String employee) {
		this.tempSaleList = tempSaleList;
		this.noSale = noSale;
		this.product = product;
		this.payType = payType;
		this.total = total;
		this.received = received;
		this.change = change;
		this.saleDate = saleDate;
		this.employee = employee;
	}

	public TemporarySale[] getTempSaleList() {
		return tempSaleList;
	}

	public void setTempSaleList(TemporarySale[] tempSaleList) {
		this.tempSaleList = tempSaleList;
	}


	public String getNoSale() {
		return noSale;
	}


	public void setNoSale(String noSale) {
		this.noSale = noSale;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getPayType() {
		return payType;
	}


	public void setPayType(String payType) {
		this.payType = payType;
	}


	public String getTotal() {
		return total;
	}


	public void setTotal(String total) {
		this.total = total;
	}


	public String getReceived() {
		return received;
	}


	public void setReceived(String received) {
		this.received = received;
	}


	public String getChange() {
		return change;
	}


	public void setChange(String change) {
		this.change = change;
	}


	public String getSaleDate() {
		return saleDate;
	}


	public void setSaleDate(String saleDate) {
		this.saleDate = saleDate;
	}


	public String getEmployee() {
		return employee;
	}


	public void setEmployee(String employee) {
		this.employee = employee;
	}


	@Override
	public String toString() {
		return "SaleTicketModel [tempSaleList=" + Arrays.toString(tempSaleList) + ", noSale=" + noSale + ", product="
				+ product + ", payType=" + payType + ", total=" + total + ", received=" + received + ", change="
				+ change + ", saleDate=" + saleDate + ", employee=" + employee + "]";
	}


	
}
