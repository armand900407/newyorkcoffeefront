package com.project.newyorkcoffee.model;

import java.io.Serializable;
import java.util.Objects;

public class Incomes implements Serializable {

	/**
	 * Serial Version.
	 */
	private static final long serialVersionUID = 2324788953342843264L;
	/**
	 * idIncome that corresponds to the idIncome.
	 */
	private Integer idIncome;
	/**
	 * noSale that corresponds to the noSale.
	 */
	private Integer noSale;
	/**
	 * description that corresponds to the product description bought.
	 */
	private String description;
	/**
	 * paymentType that corresponds to the paymentType.
	 */
	private String paymentType;
	/**
	 * total that corresponds to the paymentType.
	 */
	private Double total;
	/**
	 * paymentType that corresponds to the paymentType.
	 */
	private String incomeDate;
	/**
	 * employee that corresponds to the employee.
	 */
	private String employee;
	
	/**
	 * Constructor with Arguments for Incomes.
	 */
	public Incomes(Integer idIncome, Integer noSale, String description, String paymentType, Double total, 
			String incomeDate, String employee) {
		this.idIncome = idIncome;
		this.noSale = noSale;
		this.description = description;
		this.paymentType = paymentType;
		this.total = total;
		this.incomeDate = incomeDate;
		this.employee = employee;
	}
	
	/**
	 * Constructor with No Arguments for Incomes.
	 */
	public Incomes() {
	}

	/**
	 * hashCode.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(idIncome, noSale, description, paymentType, total, incomeDate, employee);
	}

	/**
	 * equals.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Incomes other = (Incomes) obj;
		return Objects.equals(idIncome, other.idIncome) && Objects.equals(noSale, other.noSale) 
				&& Objects.equals(description, other.description) && Objects.equals(paymentType, other.paymentType)
				&& Objects.equals(total, other.total) && Objects.equals(incomeDate, other.incomeDate)
				&& Objects.equals(employee, other.employee);
	}
	
	
	/**
	 * toString.
	 */
	@Override
	public String toString() {
		return "Incomes [idIncome=" + idIncome + ", noSale=" + noSale + ", description=" + description
				+ ", paymentType=" + paymentType+ ", total=" + total + ", incomeDate=" + incomeDate + ", employee=" + employee + "]";
	}

	public Integer getIdIncome() {
		return idIncome;
	}

	public void setIdIncome(Integer idIncome) {
		this.idIncome = idIncome;
	}

	public Integer getNoSale() {
		return noSale;
	}

	public void setNoSale(Integer noSale) {
		this.noSale = noSale;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getIncomeDate() {
		return incomeDate;
	}

	public void setIncomeDate(String incomeDate) {
		this.incomeDate = incomeDate;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public String getEmployee() {
		return employee;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}
	
	
	
}
