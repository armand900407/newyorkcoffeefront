package com.project.newyorkcoffee.print.tickets;

import java.awt.print.PrinterException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.swing.JOptionPane;

import com.project.newyorkcoffee.model.SaleTicketModel;
import com.project.newyorkcoffee.model.TemporarySale;

public class SaleTicket {
	
	public static void salePrintClientTicket(SaleTicketModel saleTicket,String client) throws PrinterException{

		 TemporarySale[] tempSaleList = saleTicket.getTempSaleList();
	     String total = saleTicket.getTotal();
	     String recibido = saleTicket.getReceived();
	     String cambio = saleTicket.getChange();
	     String tipoPago =saleTicket.getPayType();

	     ArrayList <String> listaU = new ArrayList<String>();
	     
	     for (TemporarySale tempSale : tempSaleList) {
	    	 String description;
	    	 description = tempSale.getDescription();
	    	 if(description.length()<=8) {
	    		 description = description+"-------";
	    	 }
	    	 listaU.add(description.substring(0,8)+"     "+tempSale.getQuantity().toString()+"      "+tempSale.getTotal().toString()+"\n");	 
		     
		 }
	    Iterator itU= listaU.iterator();
	    StringBuilder cadenaU= new StringBuilder();
	    while(itU.hasNext()){
	     cadenaU.append(itU.next());
	      }
	    
	     String contentTicket = "\n*** NEWYORK COFFEE CLIENT***\n"+
	    "\tVENTA DE PRODUCTO\n"+
	    "No.Venta: "+saleTicket.getNoSale()+"\n"+
	    "A nombre de: "+client+"\n"+
	    "================================\n"+
	    "Producto  Cantidad  Subtotal\n"+
	    cadenaU.toString()+"\n"+           
	    "================================"+
	    "\n             TOTAL:"+total+
	    "\n          RECIBIDO:"+recibido+
	    "\n            CAMBIO:"+cambio+"\n"+
	    "\n         TIPO PAGO:"+tipoPago+"\n"+         
	    "================================\n"+
	    "ECATEPEC DE MORELOS SECC BOSQUES\n"+
	    "BOSQUE DEL TESORO #151 ESQ. \nBOSQUE DE PINO #2\n"+
	    "LE ATENDIO:\t"+saleTicket.getEmployee()+
	    "\nFecha: "+saleTicket.getSaleDate()+
	    "\n================================\n"+
	    "GRACIAS POR SU VISITA! :) XD\n"+
	    "\n\n\n"; 
	     //Especificamos el tipo de dato a imprimir
	    //Tipo: bytes; Subtipo: autodetectado
	    DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
	    //Aca obtenemos el servicio de impresion por defatul
	    //Si no quieres ver el dialogo de seleccionar impresora usa esto
	    //PrintService defaultService = PrintServiceLookup.lookupDefaultPrintService();
	    //Si quieres ver el dialogo de seleccionar impresora usalo
	    //Solo mostrara las impresoras que soporte arreglo de bits
	    //PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
	    //PrintService printService[] = PrintServiceLookup.lookupPrintServices(flavor,null);
	    PrintService service = PrintServiceLookup.lookupDefaultPrintService();
	    //Creamos un arreglo de tipo byte
	    byte[] bytes;
	    //Aca convertimos el string(cuerpo del ticket) a bytes tal como
	    //lo maneja la impresora(mas bien ticketera :p)
	    bytes = contentTicket.getBytes();
	    //Creamos un documento a imprimir, a el se le appendeara
	    //el arreglo de bytes
	    Doc doc = new SimpleDoc(bytes,flavor,null);
	    //Creamos un trabajo de impresión
	    DocPrintJob job = service.createPrintJob();
	    //Imprimimos dentro de un try de a huevo
	   try {
	      //El metodo print imprime
	      job.print(doc, null);
	    }
	    catch (PrintException ex) { 
	             JOptionPane.showMessageDialog(null,"Revise conexion con Impresora");
	    } 
	     
	  }
	
	public static void salePrintServiceTicket(SaleTicketModel saleTicket,String client) throws PrinterException{

		 TemporarySale[] tempSaleList = saleTicket.getTempSaleList();
	     String total = saleTicket.getTotal();
	     String recibido = saleTicket.getReceived();
	     String cambio = saleTicket.getChange();
	     String tipoPago =saleTicket.getPayType();

	     ArrayList <String> listaU = new ArrayList<String>();
	     ArrayList <String> listaServicio = new ArrayList<String>();
	     
	     for (TemporarySale tempSale : tempSaleList) {
	    	 String description;
	    	 description = tempSale.getDescription();
	    	 if(description.length()<=8) {
	    		 description = description+"-------";
	    	 }
	    	 listaU.add(description.substring(0,8)+"     "+tempSale.getQuantity().toString()+"      "+tempSale.getTotal().toString()+"\n");	 
	    	 listaServicio.add(tempSale.getQuantity().toString()+"  "+tempSale.getDescription());
		 }
	     
	    Iterator itU= listaU.iterator();
	    StringBuilder cadenaU= new StringBuilder();
	    while(itU.hasNext()){
	     cadenaU.append(itU.next());
	      }
	    
	    Iterator itSer= listaServicio.iterator();
	    StringBuilder cadenaSer= new StringBuilder();
	    while(itSer.hasNext()){
	    	cadenaSer.append(itSer.next());
	      }
	    
	     String contentTicket = "\n*** NEWYORK COFFEE SERVICE***\n"+
	    "\tVENTA DE PRODUCTO\n"+
	    "No.Venta: "+saleTicket.getNoSale()+"\n"+
	    "A nombre de: "+client+"\n"+
	    "================================\n"+
	    "Producto  Cantidad  Subtotal\n"+
	    cadenaU.toString()+"\n"+           
	    "================================"+
	    "\n             TOTAL:"+total+
	    "\n          RECIBIDO:"+recibido+
	    "\n            CAMBIO:"+cambio+"\n"+
	    "\n         TIPO PAGO:"+tipoPago+"\n"+         
	    "================================\n"+
	    "Orden a atender:\n"+
	    "Cantidad        Descripcion\n"+
	    cadenaSer.toString()+"\n"+
	    "A nombre de: "+client+"\n"+
	    "\n================================\n"+
	    "ATIENDE:\t"+saleTicket.getEmployee()+
	    "\nFecha: "+saleTicket.getSaleDate()+
	    "\n\n\n"; 
	     //Especificamos el tipo de dato a imprimir
	    //Tipo: bytes; Subtipo: autodetectado
	    DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
	    //Aca obtenemos el servicio de impresion por defatul
	    //Si no quieres ver el dialogo de seleccionar impresora usa esto
	    //PrintService defaultService = PrintServiceLookup.lookupDefaultPrintService();
	    //Si quieres ver el dialogo de seleccionar impresora usalo
	    //Solo mostrara las impresoras que soporte arreglo de bits
	    //PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
	    //PrintService printService[] = PrintServiceLookup.lookupPrintServices(flavor,null);
	    PrintService service = PrintServiceLookup.lookupDefaultPrintService();
	    //Creamos un arreglo de tipo byte
	    byte[] bytes;
	    //Aca convertimos el string(cuerpo del ticket) a bytes tal como
	    //lo maneja la impresora(mas bien ticketera :p)
	    bytes = contentTicket.getBytes();
	    //Creamos un documento a imprimir, a el se le appendeara
	    //el arreglo de bytes
	    Doc doc = new SimpleDoc(bytes,flavor,null);
	    //Creamos un trabajo de impresión
	    DocPrintJob job = service.createPrintJob();
	    //Imprimimos dentro de un try de a huevo
	   try {
	      //El metodo print imprime
	      job.print(doc, null);
	    }
	    catch (PrintException ex) { 
	             JOptionPane.showMessageDialog(null,"Revise conexion con Impresora");
	    } 
	     
	  }

	

}
