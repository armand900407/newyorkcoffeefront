package com.project.newyorkcoffee.print.tickets;

import java.awt.print.PrinterException;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.swing.JOptionPane;

import com.project.newyorkcoffee.model.Closure;

public class ClosureTicket {
	
	 public static void closurePrintService(Closure closure)throws PrinterException {
	     String bodyClosureMail = bodyClosureBuilder(closure);
	     //Especificamos el tipo de dato a imprimir
	    //Tipo: bytes; Subtipo: autodetectado
	    DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
	    //Aca obtenemos el servicio de impresion por defatul
	    //Si no quieres ver el dialogo de seleccionar impresora usa esto
	    //PrintService defaultService = PrintServiceLookup.lookupDefaultPrintService();
	    //Si quieres ver el dialogo de seleccionar impresora usalo
	    //Solo mostrara las impresoras que soporte arreglo de bits
	    //PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
	    //PrintService printService[] = PrintServiceLookup.lookupPrintServices(flavor,null);
	    PrintService service = PrintServiceLookup.lookupDefaultPrintService(); 
	    //Creamos un arreglo de tipo byte
	    byte[] bytes;
	    //Aca convertimos el string(cuerpo del ticket) a bytes tal como
	    //lo maneja la impresora(mas bien ticketera :p)
	    bytes = bodyClosureMail.getBytes();
	    //Creamos un documento a imprimir, a el se le appendeara
	    //el arreglo de bytes
	    Doc doc = new SimpleDoc(bytes,flavor,null);
	    //Creamos un trabajo de impresión
	    DocPrintJob job = service.createPrintJob();
	    //Imprimimos dentro de un try de a huevo
	    try {
	      //El metodo print imprime 
	      job.print(doc, null);
	    }
	    catch (PrintException ex) { 
	            JOptionPane.showMessageDialog(null,"Revise conexion con Impresora");
	         } 
	    } 
	 
	 public static String bodyClosureBuilder(Closure closureData) {
	    	String initCash,totalCard,totalCash,totalIn,totalOut,totalGenerated,employee,closureDate;
	        String bodyClosureMail;
	    	initCash = closureData.getInitCash().toString();
	        totalCard = closureData.getTotalCard().toString();
	        totalCash = closureData.getTotalCash().toString();
	        totalIn = closureData.getTotalIn().toString();
	        totalOut = closureData.getTotalOut().toString();
	        totalGenerated = closureData.getTotalGenerated().toString();
	        closureDate = closureData.getClosureDate();
	        employee = closureData.getEmployeeName();
	 
		    bodyClosureMail = "\n*NEWYORK COFFEE ADMINISTRATION*\n"+
		    "\t CORTE DE CAJA\n"+    
		    "=============================\n"+
		    "DETALLE DE CORTE\n"+           
		    "=============================\n"+
		    "INICIO CAJA: "+initCash+
		    "\nTOTAL ENTRADAS: "+totalIn+
		    "\nTOTAL SALIDAS: "+totalOut+
		    "\nTOTAL EFECTIVO: "+totalCash+
		    "\nTOTAL TARJETA: "+totalCard+
		    "\n\n   TOTAL GENERADO: "+totalGenerated+              
		    "\n=============================\n"+
		    "ATENDIO:\t"+employee+
		    "\nFECHA: "+closureDate+
		    "\n=============================\n"+
		    "\tEXCELENTE DIA! :) XD\n"+
		    "COMENTARIOS:\n"+
		    "\n\n\n\n\n\n"; 
		  
		   return bodyClosureMail;
	    	
	    }

}
