package com.project.newyorkcoffee.catalogs;

public class PaymentTypeCatalog {

    public static final String CASH ="Efectivo";
	
	public static final String CARD ="Tarjeta";
	
	public static final String SALE_DESCRIPTION ="Venta de productos";
	
	public static final String INIT_CASH ="Inicio Caja";
	
	public static final int ID_CASH =1;
	
	public static final double LIMIT_CASH =1500;
	
	public static final String CANCEL_SALE_DESCRIPTION ="Cancelacion de no. venta: ";
	
}
