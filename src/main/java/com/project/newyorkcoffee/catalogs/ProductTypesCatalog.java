package com.project.newyorkcoffee.catalogs;

public class ProductTypesCatalog {
	
	public static final String HOT_DRINK ="Bebida Caliente";
	
	public static final String COLD_DRINK ="Bebida Fria";
	
	public static final String COCKTAIL_DRINK ="Cocktail";
	
	public static final String FOOD ="Comida";
	
	public static final String DRINK_COMPLEMENT ="Complemento Bebida";
	
	public static final String FOOD_COMPLEMENT ="Complemento Comida";

}
