package com.project.newyorkcoffee.catalogs;

public class ProductSizeCatalog {
	
	public static final String SMALL_SIZE ="Chico";
	
	public static final String MEDIUM_SIZE ="Mediano";
	
	public static final String BIG_SIZE ="Grande";
	
	public static final String X_BIG_SIZE ="Extra Grande";
	
	public static final String NORMAL ="Normal";
	
}
